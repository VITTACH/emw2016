#include "eventhandler.h"

#include <QDir>
#include <QFile>
#include <QClipboard>
#include <QJsonArray>
#include <QJsonObject>
#include <QApplication>
#include <QJsonDocument>
#include <QStandardPaths>
#include <QCoreApplication>

int EventHandler::getId() {
    return user.getId();
}

EventHandler::~EventHandler() {
    citysArray.clear();
    eventsArray.clear();
    membersArray.clear();
    for(int i=0; i<5;i++)
        speakersVectArray[i].clear();
}

int EventHandler::getDialog(int i) {
    return myDialogs[i].index;
}

int EventHandler::getMemberIndex() {
    return currentMemberIndex;
}

void EventHandler::readMessage(int index) {
    myDialogs[index].readed = true;
}

void EventHandler::readMsgChat(int index) {
    for(int i=0; i<myDialogs.size(); i++) {
        if (myDialogs[i].index == index) {
            myDialogs[i].readed = true;
            break;
        }
    }
}

void EventHandler::copyText(QString text) {
     QClipboard *clipboard = QApplication::clipboard();
     clipboard->clear();
     clipboard->setText(text);
}

bool EventHandler::postSpeaker(QString msgs, QString name) {
    string imsg = msgs.toStdString();
    string iname= name.toStdString();
    QUrl siteUrl = domain + "quest";

    QByteArray requestString = ("quest=" + imsg +
                                "&speaker="+ iname).c_str();

    httpPost.setUrl(&siteUrl);
    user.setResponse(httpPost.sendPost(&requestString).toStdString());

    response = user.getResponse();

    qDebug() << "postSpeaker " << response.c_str();

    QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
    QJsonObject itemObject = itemDoc.object();
    if (itemObject["result"].toInt() == 200) {
        return true;
    }
    return false;
}

//-----------------------------------------------------------------------------------------------------------------------

bool EventHandler::getMessageFlag(int index) {
return membersArray[index].getMessageFlag();
}

void EventHandler::setMessage(QString message, int index, bool flag) {
    membersArray[index].setMessage(message.toStdString(), flag);
}

//-----------------------------------------------------------------------------------------------------------------------

bool EventHandler::isPined() {
    return isPin;
}

int EventHandler::getGender() {
    return user.getGender();
}

bool EventHandler::itWasInChat() {
    return wasInChat;
}

bool EventHandler::itIsAndroid() {
    return isAndroid;
}

bool EventHandler::isManyMembers() {
    return ismanymembers;
}

//-----------------------------------------------------------------------------------------------------------------------

void EventHandler::setInChat(bool state) {
    wasInChat = state;
}

void EventHandler::setCity(QString city) {
    user.setCity(city.toStdString());
}

void EventHandler::setMail(QString mail) {
    user.setMail(mail.toStdString());
}

void EventHandler::setPost(QString post) {
    user.setPost(post.toStdString());
}

void EventHandler::setGender(int gender) {
    user.setGender(gender);
}

void EventHandler::setDescrip(QString des) {
    user.setDescrip(des.toStdString());
}

void EventHandler::setPhone(QString phone) {
    user.setNumber(phone.toStdString());
}

void EventHandler::setLogin(QString login) {
    user.setUserName(login.toStdString());
}

void EventHandler::setPicture(QString pic) {
    user.setPicture(pic.toStdString());
}

void EventHandler::setPass(QString mypass) {
    user.setPassword(mypass.toStdString());
}

void EventHandler::setFamily(QString fami) {
    user.setLastName(fami.toStdString());
}

void EventHandler::setCompany(QString com) {
    user.setCompany(com.toStdString());
}

void EventHandler::curMemberIndex(int ind) {
    currentMemberIndex = ind;
}

//-----------------------------------------------------------------------------------------------------------------------

QString EventHandler::getMail() {
    return QString(user.getMail().c_str());
}

QString EventHandler::getPost() {
    return QString(user.getPost().c_str());
}

QString EventHandler::getCity() {
    return QString(user.getCity().c_str());
}

QString EventHandler::getPhone(){
    return QString(user.getNumber().c_str() );
}

QString EventHandler::getMessage(int index, int ver) {
    if (membersArray.size() > 0)
    return QString(membersArray[index].getMessage(ver).c_str());
    return "";
}

QString EventHandler::getDescrip() {
    return QString(user.getDescrip().c_str());
}

QString EventHandler::getPicture() {
    return QString(user.getPicture().c_str());
}

QString EventHandler::getCompany() {
    return QString(user.getCompany().c_str());
}

QString EventHandler::getPass() {
    return QString(user.getPassword().c_str());
}

QString EventHandler::getLogin() {
    return QString(user.getUserName().c_str());
}

QString EventHandler::getFamily() {
    return QString(user.getLastName().c_str());
}

//-----------------------------------------------------------------------------------------------------------------------

int EventHandler::findMemberById(int id) {
    bool ifind=false;
    for(int i=0;i<membersArray.size();i++)
        if (membersArray[i].getId() == id) {
            for(int j=0; j<myDialogs.size();j++)
                if (myDialogs[j].index==i) {
                    ifind = true;
                    myDialogs[j].readed = false;
                }
            if (!ifind) myDialogs.push_back(Dialogs(i,false));
            return i;
        }
    return -1;
}

void EventHandler::fromHistory(int id, QString last_name, QString avatar, QString post, QString company, QString first_name, QString phone, QString email, QString info, int gender) {
    User newUser;
    newUser.setId(id);
    newUser.setGender(gender);
    newUser.setPost(post.toStdString());
    newUser.setMail(email.toStdString());
    newUser.setNumber(phone.toStdString());
    newUser.setDescrip(info.toStdString());
    newUser.setCompany(company.toStdString());
    newUser.setLastName(last_name.toStdString());
    newUser.setUserName(first_name.toStdString());
    if (avatar == "false") {
        if(newUser.getGender() == 0)
            newUser.setPicture("ui/profiles/default/man.png");
        else
            newUser.setPicture("ui/profiles/default/woman.png");
    }
    else
        newUser.setPicture("image://imageprovider/" + (domain + avatar).toStdString());

    //newUser.getPicture();
    membersArray.push_back(newUser);
    myDialogs.push_back(Dialogs(membersArray.size() - 1,false));
}

int EventHandler::getMemberById(int id) {

    QUrl qUrlnk = domain + "/app/user/" + QString::number(id);

    httpGet.setUrl(&qUrlnk);
    response = httpGet.sendGet().toStdString();

    qDebug() << "getMemberById " << response.c_str();

    QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
    QJsonObject itemObject = itemDoc.object();
    QJsonObject itemsObjectAboutUsers = itemObject["user"].toObject();

    if (itemObject["result"].toInt() == 200) {

    User newUser;
    newUser.setId(itemsObjectAboutUsers["id"].toString().toInt());
    newUser.setGender(itemsObjectAboutUsers["gender"].toString().toInt());
    newUser.setPost(itemsObjectAboutUsers["post"].toString().toStdString());
    newUser.setMail(itemsObjectAboutUsers["email"].toString().toStdString());
    newUser.setDescrip(itemsObjectAboutUsers["info"].toString().toStdString());
    if (itemsObjectAboutUsers["avatar"].toString() == "false") {
        if(newUser.getGender() == 0)
            newUser.setPicture("ui/profiles/default/man.png");
        else
            newUser.setPicture("ui/profiles/default/woman.png");
    }
    else
        newUser.setPicture("image://imageprovider/" + (domain + itemsObjectAboutUsers["avatar"].toString()).toStdString());
    newUser.setNumber(itemsObjectAboutUsers["phone"].toString().toStdString());
    newUser.setCompany(itemsObjectAboutUsers["company"].toString().toStdString());
    newUser.setLastName(itemsObjectAboutUsers["last_name"].toString().toStdString());
    newUser.setUserName(itemsObjectAboutUsers["first_name"].toString().toStdString());

    /*
    for(int i = 0; i < membersArray.size(); i++)
    if(membersArray[i].getId()==newUser.getId())
        return i;
    */
    //newUser.getPicture();
    membersArray.push_back(newUser);

        myDialogs.push_back(Dialogs(membersArray.size() - 1,false));
    }
    return membersArray.size() - 1;
}

int EventHandler::getMemberId(int ver) {
    if(ver == 0)
    return membersArray[currentMemberIndex].getId();
    else
        return speakersVectArray[ver-1][currentMemberIndex].getId();
}

QString EventHandler::getMemberMail(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getMail().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getMail().c_str());
}

QString EventHandler::getMemberPost(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getPost().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getPost().c_str());
}

QString EventHandler::getMemberPhone(int ver){
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getNumber().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getNumber().c_str());
}

QString EventHandler::getMemberPicture(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getPicture().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getPicture().c_str());
}

QString EventHandler::getMemberDescrip(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getDescrip().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getDescrip().c_str());
}

QString EventHandler::getMemberCompany(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getCompany().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getCompany().c_str());
}

QString EventHandler::getMemberLogin(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getUserName().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getUserName().c_str());
}

QString EventHandler::getMemberFamily(int ver) {
    if(ver == 0)
    return QString(membersArray[currentMemberIndex].getLastName().c_str());
    else
        return QString(speakersVectArray[ver-1][currentMemberIndex].getLastName().c_str());
}

//-----------------------------------------------------------------------------------------------------------------------

EventHandler::EventHandler(QObject *parent) : QObject(parent) {
    if(!isAndroid) {
    QString path = QStandardPaths::standardLocations(QStandardPaths::DataLocation).value(0);
    QDir dir(path);
    if (!dir.exists())
    dir.mkpath(path);
    if (!path.isEmpty() && !path.endsWith("/"))
    path+="/"+PathFile;
    PathFile= path;
    }
}

// выбирать по очереди по одному из мероприятий из vector и возвращать имя
QString EventHandler::takeEvent(int variable) {
    if (currentEvent < eventsArray.size())
    switch(variable) {
    case -1: currentEvent++;return QString("");
    case 2:
        return QString(eventsArray[currentEvent].getMail().c_str());
    case 1:
        return QString(eventsArray[currentEvent].getTheme().c_str());
    case 0:
        return QString(eventsArray[currentEvent].getPicture().c_str());
    }
    return "FATAL_ERROR";
}

// выбирать по очереди по одному из участников из vector и возвращать имя
QString EventHandler::takeMember(int variable) {
    if (currentMember < membersArray.size() && currentMember < (curMemberPage + 1) * 20)
    switch(variable) {
    case -1: currentMember++;return QString("");
    case 2:
        return QString(membersArray[currentMember].getPost().c_str());
    case 5:
        return QString(membersArray[currentMember].getTime().c_str());
    case 0:
        return QString(membersArray[currentMember].getPicture().c_str());
    case 3:
        return QString(membersArray[currentMember].getCompany().c_str());
    case 1:
        return QString(membersArray[currentMember].getUserName().c_str());
    case 4:
        return QString(membersArray[currentMember].getLastName().c_str());
    }
    return "FATAL_ERROR";
}

// выбирать по очереди по одному из спикеров из vector и возвращать имя
QString EventHandler::takeSpeaker(int page,int variable) {
    if (currentSpeaker[page] < speakersVectArray[page].size())
    switch(variable) {
    case -1: currentSpeaker[page]++; return QString("");
    case 4:
        return QString(speakersVectArray[page][currentSpeaker[page]].getTime().c_str());
    case 5:
        return QString(speakersVectArray[page][currentSpeaker[page]].getTheme().c_str());
    case 1:
        return QString((speakersVectArray[page][currentSpeaker[page]].getUserName()).c_str());
    case 2:
        return QString(speakersVectArray[page][currentSpeaker[page]].getPost().c_str());
    case 3:
        return QString(speakersVectArray[page][currentSpeaker[page]].getCompany().c_str());
    case 0:
        return QString(speakersVectArray[page][currentSpeaker[page]].getPicture().c_str());
    }
    return "FATAL_ERROR";
}

// выбирать по очереди по одному элементу из списка диалогов и возвращать
QString EventHandler::takeDialog(int variable) {
    if (variable == -2) { currentDialog = 0; return QString(""); }
    if (currentDialog < myDialogs.size())
    switch(variable) {
    case -1: currentDialog++;return QString("");
    case 4:
        return QString(myDialogs[currentDialog].readed? "1": "0");
    case 3:
    return QString(membersArray[myDialogs[currentDialog].index].getEndMessage().c_str());
    case 5:
    return QString(membersArray[myDialogs[currentDialog].index].getEndMessageFlag()?"1": "0");
    case 2:
        return QString(membersArray[myDialogs[currentDialog].index].getCompany().c_str());
    case 1:
        return QString((membersArray[myDialogs[currentDialog].index].getUserName() + " " + membersArray[myDialogs[currentDialog].index].getLastName()).c_str());
    case 0:
        return QString(membersArray[myDialogs[currentDialog].index].getPicture().c_str());
    }
    return "FATAL_ERROR";
}

bool EventHandler::updatedUsers(QString login, QString family, QString post, QString comp, QString phone, QString info, QString city) {
    string iinfo = info.toStdString();
    string icity = city.toStdString();
    string iname = login.toStdString();
    string ifamil =family.toStdString();
    string ipost = post.toStdString();
    string icomp = comp.toStdString();
    string iphone = phone.toStdString();
    QUrl qUrlink = domain + "app/user/update/info";

    QByteArray requestString = ("phone=" + iphone +
                                "&last_name=" + ifamil +
                                "&first_name=" + iname +
                                "&post=" + ipost +
                                "&info=" + iinfo +
                                "&city=" + icity +
                                "&company=" + icomp).c_str();

    httpPost.setUrl(&qUrlink);
    user.setResponse(httpPost.sendPost(&requestString).toStdString());

    response=user.getResponse();

    qDebug() << "updateInfo " << response.c_str();

    return  parseReadResponse();
}

bool EventHandler::parseReadResponse() {
    if (response.length() > 0) {
        QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
        QJsonObject itemObject = itemDoc.object();
        QJsonObject itemsObjectAboutUsers = itemObject["user"].toObject();

        if (itemObject["result"].toInt() == 200) {
            user.setSsid(httpPost.sessID);
            user.setId(itemsObjectAboutUsers["id"].toInt());
            user.setMail(itemsObjectAboutUsers["email"].toString().toStdString());
            user.setCompany(itemsObjectAboutUsers["company"].toString().toStdString());
            user.setLastName(itemsObjectAboutUsers["last_name"].toString().toStdString());
            user.setUserName(itemsObjectAboutUsers["first_name"].toString().toStdString());
            user.setGender(itemsObjectAboutUsers["gender"].toString()=="1"? 1:0);
            user.setPost(itemsObjectAboutUsers["post"].toString().toStdString());
            user.setCity(itemsObjectAboutUsers["city"].toString().toStdString());
            user.setDescrip(itemsObjectAboutUsers["info"].toString().toStdString());
            if (itemsObjectAboutUsers["avatar"].toString() == "false"){
                if (user.getGender() == 0){user.setPicture("ui/profiles/default/man.png");}
                else{user.setPicture("ui/profiles/default/woman.png");}
            }
            else
            user.setPicture("image://imageprovider/" + (domain + itemsObjectAboutUsers["avatar"].toString()).toStdString());
            savingToFile(QString(user.getNumber().c_str()), QString(user.getPassword().c_str()));
        return 1;
        }
    }
    return false;
}

void EventHandler::savingToFile(QString phone,QString mypass) {
    QFile file(PathFile);
    if (file.open(QIODevice::WriteOnly)) {
        file.write(QString("phone=" + phone + ";password=" + mypass/*+ ";avatar=" + QString::fromStdString(user.getPicture())*/).toStdString().c_str());
    }
    file.close();
}

bool EventHandler::applications(QString name, QString phone, QString email, QString desc) {
    string idesc = desc.toStdString();
    string iname = name.toStdString();
    string iphone = phone.toStdString();
    string iemail = email.toStdString();
    QUrl siteUrl= domain + "application_order";

    QByteArray requestString = ("name=" + iname +
                                "&email="+ iemail +
                                "&phone="+ iphone +
                                "&desc=" + idesc).c_str();

    httpPost.setUrl(&siteUrl);
    user.setResponse(httpPost.sendPost(&requestString).toStdString());

    response = user.getResponse();

    qDebug() << "applications " << response.c_str();

    QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
    QJsonObject itemObject = itemDoc.object();
    if (itemObject["result"].toInt() == 200) {
        return true;
    }
    return false;
}

int EventHandler::registration(QString login, QString family, QString post, QString company, QString email, QString phone, QString city, bool gender) {
    string ipost = post.toStdString();
    string icity = city.toStdString();
    string iname = login.toStdString();
    string igender = gender ? "1" : "0";
    string iphone = phone.toStdString();
    string iemail = email.toStdString();
    string ifamil =family.toStdString();
    string icompany = company.toStdString();
    QUrl siteUrl= domain + "app/auth/registration";

    QByteArray requestString = ("phone=" + iphone +
                                "&email=" + iemail +
                                "&last_name=" + ifamil +
                                "&first_name=" + iname +
                                "&post=" + ipost +
                                "&city=" + icity +
                                "&company=" + icompany +
                                "&gender=" + igender).c_str();

    httpPost.setUrl(&siteUrl);
    user.setResponse(httpPost.sendPost(&requestString).toStdString());

    response = user.getResponse();

    qDebug() << "Registration " << response.c_str();

    QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
    QJsonObject itemObject = itemDoc.object();
    user.setPassword(itemObject["password"].toString().toStdString());

    if(response!="")
    return networkLogin(phone, itemObject["password"].toString())?1:0;
    return -1;
}

// залогиниться в приложухе, либо через login.qml с параметрами, либо через basic.qml без них
int EventHandler::networkLogin(QString phoneField,QString passField){
    string password;
    string username;
    QFile file(PathFile);
    QString memberString;
    QUrl qUrl = domain + "app/auth/authorization";

    if(passField != "" && phoneField != "") {
        user.setPassword(password= passField.toStdString());
        user.setNumber(username = phoneField.toStdString());
    }
    else if(file.open(QIODevice::ReadOnly)) {
        if ((memberString = file.readAll()).size() !=  0) {
            QStringList keyValPairs=memberString.split(";");
            foreach (QString encodedPair, keyValPairs)
            {
                QStringList keyVal = encodedPair.split("=");
                if (keyVal[0] == "phone")
                user.setNumber(username = keyVal.at(1).toStdString());
                else if (keyVal[0] == "password")
                user.setPassword(password=keyVal.at(1).toStdString());
            }
        }
        file.close();
    }

    QByteArray requestString = ("phone=" + username + "&password=" + password).c_str();

    httpPost.setUrl(&qUrl);
    user.setResponse(httpPost.sendPost(&requestString).toStdString());

    response=user.getResponse();

    qDebug() << "Login " << response.c_str();

    if(response!="")
    return parseReadResponse()? 1:0;
    return -1;
}

// получить весь список мероприятий из базы и расрапсить его в vector
void EventHandler::getEvents() {
    currentEvent = 0;
    if(eventsArray.empty()==1) {

        QUrl qUrlnk = domain + "app/events/eventList";

        httpGet.setUrl(&qUrlnk);
        response = httpGet.sendGet().toStdString();

        qDebug()<<"getEvents " << response.c_str();

        QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
        QJsonObject itemObject = itemDoc.object();
        QJsonArray jsonArray= itemObject["events"].toArray();

        foreach (const QJsonValue &value, jsonArray) {
        User newUser;
        QJsonObject ob=value.toObject();
        newUser.setId(ob["id"].toString().toInt());
        newUser.setTheme(ob["desc"].toString().toStdString());
        newUser.setMail(ob["site_url"].toString().toStdString());
        if (ob["event_theme"].toString() == "false")
            newUser.setPicture("ui/appIcon/iconMax.png");
        else
        newUser.setPicture("image://imageprovider/"+ (domain + ob["event_theme"].toString()).toStdString());
        //newUser.getPicture();
        eventsArray.push_back(newUser);
        }
    }
}

// получить весь список спикеров из базы и расрапсить его в vector
void EventHandler::getSpeakers(int pageNumber,int size) {
    if (speakersVectArray[pageNumber - 1].empty()) {
        currentSpeaker[pageNumber-1] = 0;
        QUrl qUrlnk = domain + "app/events/speakersList";

        httpPost.setUrl(&qUrlnk);
        QByteArray requestString;
        requestString = ("page=" + QString::number(pageNumber).toStdString()).c_str();

        response = httpPost.sendPost(&requestString).toStdString();

        qDebug() << "getSpeakers " << response.c_str();

        QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
        QJsonObject itemObject = itemDoc.object();
        QJsonArray jsonArray= itemObject["speakers"].toArray();

        foreach (const QJsonValue &value, jsonArray) {
        User newUser;
        QJsonObject ob=value.toObject();
        newUser.setId(ob["id"].toString().toInt());
        newUser.setTime(ob["event_time"].toString().toStdString());
        newUser.setGender(ob["speaker_gender"].toString().toInt());
        newUser.setPost(ob["speaker_post"].toString().toStdString());
        newUser.setTheme(ob["event_title"].toString().toStdString());
        newUser.setMail(ob["speaker_email"].toString().toStdString());
        newUser.setUserName(ob["speaker_name"].toString().toStdString());
        if (ob["speaker_avatar"].toString() == "false") {
            if(newUser.getGender() == 0)
                newUser.setPicture("ui/profiles/default/man.png");
            else
                newUser.setPicture("ui/profiles/default/woman.png");
        }
        else {
            if(newUser.getId()==33)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==35)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==36)
                newUser.setPicture("ui/speakers/davidov.jpg");
            else if(newUser.getId()==37)
                newUser.setPicture("ui/speakers/berezin.jpg");
            else if(newUser.getId()==38)
                newUser.setPicture("ui/speakers/bugaev.jpg");
            else if(newUser.getId()==39)
                newUser.setPicture("ui/speakers/udovenko.jpg");
            else if(newUser.getId()==40)
                newUser.setPicture("ui/speakers/Zueva.jpg");
            else if(newUser.getId()==41)
                newUser.setPicture("ui/speakers/murazanov.jpg");
            else if(newUser.getId()==42)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==80)
                newUser.setPicture("ui/speakers/klepikov.jpg");
            else if(newUser.getId()==43)
                newUser.setPicture("ui/speakers/subbotin.png");
            else if(newUser.getId()==44)
                newUser.setPicture("ui/speakers/konovalov.jpg");
            else if(newUser.getId()==45)
                newUser.setPicture("ui/speakers/shabat.jpg");
            else if(newUser.getId()==46)
                newUser.setPicture("ui/speakers/balakirev.png");
            else if(newUser.getId()==47)
                newUser.setPicture("ui/speakers/bajenov.jpg");
            else if(newUser.getId()==48)
                newUser.setPicture("ui/speakers/ashomko.jpg");
            else if(newUser.getId()==49)
                newUser.setPicture("ui/speakers/kotlyarov.jpg");
            else if(newUser.getId()==50)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==51)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==52)
                newUser.setPicture("ui/speakers/balakirev.png");
            else if(newUser.getId()==53)
                newUser.setPicture("ui/speakers/vlasova.jpg");
            else if(newUser.getId()==54)
                newUser.setPicture("ui/speakers/obvinceva.png");
            else if(newUser.getId()==55)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==56)
                newUser.setPicture("ui/speakers/nahabo.jpg");
            else if(newUser.getId()==57)
                newUser.setPicture("ui/speakers/u_davidov.jpg");
            else if(newUser.getId()==58)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==59)
                newUser.setPicture("ui/speakers/benimana.jpg");
            else if(newUser.getId()==60)
                newUser.setPicture("ui/speakers/anurova.jpg");
            else if(newUser.getId()==61)
                newUser.setPicture("ui/speakers/filipenkova.jpg");
            else if(newUser.getId()==62)
                newUser.setPicture("ui/speakers/berezin.jpg");
            else if(newUser.getId()==63)
                newUser.setPicture("ui/speakers/bobrova.jpg");
            else if(newUser.getId()==64)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==65)
                newUser.setPicture("ui/speakers/kuchkarov.jpg");
            else if(newUser.getId()==66)
                newUser.setPicture("ui/speakers/lebedeva.jpg");
            else if(newUser.getId()==67)
                newUser.setPicture("ui/speakers/polewuk.jpg");
            else if(newUser.getId()==68)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==81)
                newUser.setPicture("ui/speakers/menshikov.jpg");
            else if(newUser.getId()==82)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==83)
                newUser.setPicture("ui/speakers/anurova.jpg");
            else if(newUser.getId()==84)
                newUser.setPicture("ui/speakers/anurova.jpg");
            else if(newUser.getId()==85)
                newUser.setPicture("ui/speakers/anurova.jpg");
            else if(newUser.getId()==86)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==87)
                newUser.setPicture("ui/speakers/anurova.jpg");
            else if(newUser.getId()==69)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==70)
                newUser.setPicture("ui/speakers/barabanchikov.jpg");
            else if(newUser.getId()==71)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==72)
                newUser.setPicture("ui/speakers/sergeev.jpg");
            else if(newUser.getId()==73)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==74)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==75)
                newUser.setPicture("ui/speakers/polewuk.jpg");
            else if(newUser.getId()==76)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==77)
                newUser.setPicture("ui/speakers/default_img.png");
            else if(newUser.getId()==78)
                newUser.setPicture("ui/speakers/ponomarenko.jpg");
            else if(newUser.getId()==79)
                newUser.setPicture("ui/speakers/default_img.png");
            else
                newUser.setPicture("ui/speakers/default_img.png");
            /*
            else
            newUser.setPicture("image://imageprovider/" + (domain + ob["speaker_avatar"].toString()).toStdString());
            /*
            #include <stdlib.h>
            newUser.setPicture("image://imageprovider/http://lorempixel.com/500/500/?seed=" + QString::number(rand()%1000).toStdString());
            // example saving image from binary code
            #include <QImage>
            QByteArray bay = QByteArray::fromBase64(ob["avatar"].toString().toUtf8());
            QImage img;
            img= QImage::fromData(bay, "PNG");
            img.save(QString::number(newUser.getId()) + ".png");
            newUser.setPicture(QString::number(newUser.getId()).toStdString()+".png");
            */
        }
        //newUser.getPicture();
        newUser.setNumber(ob["speaker_phone"].toString().toStdString());
        newUser.setCompany(ob["speaker_company"].toString().toStdString());
        speakersVectArray[pageNumber-1].push_back(newUser);
        }
    }
    else if (size == 0) {
        currentSpeaker[pageNumber - 1] = 0; return;
    }
}

// получить весь список участников из базы и расрапсить его в vector
void EventHandler::getMembers(int pageNumber,int pageIndex) {
    bool repeat= false;
    if(!isManyMembers()
            && membersArray.size() > 0
            && !wasInChat)
        currentMember = 0;
    else {
        if (pageIndex == 0) {
            currentMember = 0;
            if (pageNumber > 0 || isManyMembers())
                return;
        }
        curMemberPage=pageNumber;
        QUrl qUrlnk= domain + "app/user/userList";

        if (membersArray.size() > 0 && !wasInChat)
            membersArray.pop_back();

        httpPost.setUrl(&qUrlnk);
        QByteArray requestString;
        requestString = ("page=" + QString::number(pageNumber).toStdString()).c_str();

        response = httpPost.sendPost(&requestString).toStdString();

        qDebug() << "getMembers " << response.c_str();

        QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
        QJsonObject itemObject = itemDoc.object();
        QJsonArray jsonArray= itemObject["users"].toArray();

        ismanymembers= (jsonArray.size() > 20)? true: false;

        foreach (const QJsonValue &value, jsonArray) {
        User newUser;
        QJsonObject ob=value.toObject();
        newUser.setId(ob["id"].toString().toInt());
        newUser.setGender(ob["gender"].toString().toInt());
        newUser.setPost(ob["post"].toString().toStdString());
        newUser.setMail(ob["email"].toString().toStdString());
        newUser.setDescrip(ob["info"].toString().toStdString());
        if (ob["avatar"].toString() == "false") {
            if(newUser.getGender() == 0)
                newUser.setPicture("ui/profiles/default/man.png");
            else
                newUser.setPicture("ui/profiles/default/woman.png");
        }
        else {
            newUser.setPicture("image://imageprovider/" + (domain + ob["avatar"].toString()).toStdString());
            /*
            #include <stdlib.h>
            newUser.setPicture("image://imageprovider/http://lorempixel.com/500/500/?seed=" + QString::number(rand()%1000).toStdString());
            // example saving image from binary code
            #include <QImage>
            QByteArray bay = QByteArray::fromBase64(ob["avatar"].toString().toUtf8());
            QImage img;
            img= QImage::fromData(bay, "PNG");
            img.save(QString::number(newUser.getId()) + ".png");
            newUser.setPicture(QString::number(newUser.getId()).toStdString()+".png");
            */
        }
        newUser.setTime(ob["day"].toString().toStdString());
        newUser.setNumber(ob["phone"].toString().toStdString());
        newUser.setCompany(ob["company"].toString().toStdString());
        newUser.setLastName(ob["last_name"].toString().toStdString());
        newUser.setUserName(ob["first_name"].toString().toStdString());

        //newUser.getPicture();
        for(int i = 0; i < membersArray.size(); i++)
        if(membersArray[i].getId()==newUser.getId())
        {
            repeat = true;
            break;
        }
        if(repeat) {
            repeat = false;
            continue;
        }
        membersArray.push_back(newUser);
        }
    }
}

// отсылать на сервер выбранную на устройстве фотографию по заданному пути
void EventHandler::sendAvatar(QString picsPath) {
    // remove first three symbols as "qrc"
    //picsPath = QString::fromStdString(picsPath.toStdString().substr(4));
    QFile fi(QUrl(picsPath).toLocalFile());
    QUrl qUrl = domain + "app/user/uploadAvatar";

    httpPost.setUrl(&qUrl);
    response=httpPost.sendAvatar(&fi).toStdString();

    qDebug() << "sendingAvatar" << response.c_str();
}

// выбирать по очереди по одному из городов из vector и возвращать его имя
QString EventHandler::takeCity() {
    if (currentCity < citysArray.size()) return citysArray[currentCity++];
    return "FATAL_ERROR";
}

// получить весь список городов из базы и расрапсить его в vector
void EventHandler::getCitys() {
    currentCity = 0;
    if(citysArray.empty() == true) {
        QUrl qUrl = domain + "geo/getCities";

        httpPost.setUrl(&qUrl);
        QByteArray requestString = "";

        response = httpPost.sendPost(&requestString).toStdString();

        QJsonDocument itemDoc = QJsonDocument::fromJson(response.c_str());
        QJsonObject itemObject = itemDoc.object();
        QJsonArray jsonArray= itemObject["cities"].toArray();

        foreach (const QJsonValue &value, jsonArray) {
            QJsonObject obj = value.toObject();
                citysArray.push_back(obj["name"].toString());
        }
        qDebug()<<"getCitys " << response.c_str();
    }
}

// выход из своего аккаунта и разлогинивание и удаление сейва
void EventHandler::exitMenu() {
    QUrl qUrl = domain + "app/logout";
    QFile ifile(PathFile);
    ifile.setPermissions(QFile::ReadOther|QFile::WriteOther);
    ifile.remove();
    httpGet.setUrl(&qUrl);
    httpGet.sendGet();
    user.setSsid("");
}
