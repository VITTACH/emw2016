import QtQuick 2.7
import QtWebSockets 1.0
import QtMultimedia 5.6
import QtQuick.Controls 2.0

ApplicationWindow {
    id: root
    width: 500
    height: 900
    visible: true
    title: qsTr("EMW_2016")

    function strartPage() {
        privated.visitedPagesList.push(loader.source = "qrc:/basic.qml")
    }

    Timer {
        repeat: true
        running: true
        interval: 100
        onTriggered: {
            var id=event_handler.getId()
            if(id > 0)
            {
                if(!socket.active)
                    socket.active = true
                socket.url = "ws://forum.app-labs.ru:8080?user_id=" + id;
            }
        }
    }

    QtObject {
        id: facade
        function toPx(dp) {
            return dp * (loader.dpi/160)
        }
        function doPx(dp) {
            return dp * (loader.dpi/160) * 1.5
        }
    }

    Loader {
        id: loader
        focus:true
        objectName: "loader"
        anchors.fill: parent

        property bool reloadDialogs: false
        property real dpi: 0
        property bool reloadChat: false
        property variant fields: ["","","","","","","",""]

        property int chatVariable: 0
        property int pageMemberIndex: 0
        property int currentDateList: 0
        property int drawerCurrentIndex:0
        property int currentChatIndex: -2

        Keys.onReleased: {
            if (event.key==Qt.Key_Back
                    ||event.key==Qt.Key_Escape) {
                loader.back();
                event.accepted = true;
            }
        }

        QtObject {
            id: privated
            property var visitedPagesList:[]
        }

        Component.onCompleted: strartPage();

        function goTo(page) {
            privated.visitedPagesList.push(source=page)
        }
        function back() {
            if (privated.visitedPagesList.length > 0) {
                if (source != "qrc:/profile.qml") {
                    privated.visitedPagesList.pop()
                    source = privated.visitedPagesList[privated.visitedPagesList.length - 1];
                }
            }
            else
                strartPage();
        }
    }

    FontLoader {
        id: museoSansMidle
        source: "qrc:/fonts/MuseoSansMidle.ttf"
    }
    FontLoader {
        id: museoSansLight
        source: "qrc:/fonts/MuseoSansLight.ttf"
    }

    function convertFromJSON(msg) {
        var id;
        var obj = JSON.parse(msg)
        if(obj.type == "message") {
            id = obj.data.from.id
            console.log("remsg= "+obj.data.msg.msg_text+" ID= " + id)
            loader.currentChatIndex= event_handler.findMemberById(id)
            if(loader.currentChatIndex<0)
            loader.currentChatIndex = event_handler.getMemberById(id)
            return obj.data.msg.msg_text;
        }
        else if(obj.type == "chats_history") {
            for(var i=0; i<obj.data.length; i++) {
                event_handler.fromHistory(obj.data[i].partner_info.id,
                                          obj.data[i].partner_info.last_name,
                                          obj.data[i].partner_info.avatar,
                                          obj.data[i].partner_info.post,
                                          obj.data[i].partner_info.company,
                                          obj.data[i].partner_info.first_name,
                                          obj.data[i].partner_info.phone,
                                          obj.data[i].partner_info.email,
                                          Qt.atob(obj.data[i].partner_info.info),
                                          obj.data[i].partner_info.gender
                                          )
                for(var j=obj.data[i].messages.length-1; j>=0; j--) {
                    event_handler.setMessage(obj.data[i].messages[j].message_text,
                                             i,
                                             obj.data[i].messages[j].msg_type=="1"?true:false)
                }
            }
            event_handler.setInChat(true)
            return "FATAL_ERROR"
        }

        return "FATAL_ERROR"
    }

    SoundEffect {
        id: playSound
        source: "qrc:/sounds/message.wav"
    }

    WebSocket {
        id: socket
        active: false
        onTextMessageReceived: {
            var msg = convertFromJSON(message)
            if(msg!="FATAL_ERROR") {
            playSound.play()
            loader.chatVariable = 0;
            loader.reloadDialogs = true;
            var ind = loader.currentChatIndex;
            event_handler.setMessage(msg,ind);
            }
        }
        onStatusChanged: {
            if (socket.status == WebSocket.Error) {
                console.log(qsTr("error:%1").arg(socket.errorString))
            } else if (socket.status == WebSocket.Open) {
                console.log("Client open. id="+event_handler.getId())
            } else if (socket.status == WebSocket.Closed) {
                console.log(qsTr("Client socket closed."));
                /*
                privated.visitedPagesList = []
                loader.goTo("qrc:/login.qml");
                */
            } else if (socket.status == WebSocket.Connecting) {
                console.log(qsTr("Client socket Connecting."));
            }
        }
    }
}
