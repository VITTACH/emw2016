import QtQuick 2.7
//import QtQuick.Dialogs 1.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootProfile

    EmwStyle.NavigateDrawer
    {id: profileScreenDrawer}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(350)
        id: profileScreenNavbarMenu

        Image {
            x: rootProfile.width/2 - width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootProfile.width<rootProfile.height/1.5)?
                    rootProfile.height/2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootProfile.width<rootProfile.height/1.5)?
                    sourceSize.width*(rootProfile.height/2/sourceSize.height):
                        rootProfile.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton
            width:2*hambrButtonImage.width;
            height:hambrButtonImage.height;
            background:Rectangle{opacity:0}

            onClicked: profileScreenDrawer.open()

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Button {
            y: facade.toPx(41)
            id: profileEditButton
            width: editButtonImage.width;
            height:editButtonImage.height
            background:Rectangle{opacity: 0}

            anchors {
                right: parent.right
                rightMargin: facade.toPx(20)
            }

            onClicked:{
                loader.goTo("qrc:/settings.qml")
            }

            Image {
                id: editButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode : Image.PreserveAspectFit;
                source: "ui/buttons/editButton.png"
            }
        }

        Rectangle {
            color: "transparent"
            width: parent.width;
            height: parent.height - avatarButton.height/2

            Column {
                spacing: facade.toPx(30)
                anchors {
                    centerIn: parent
                    leftMargin: profileDrawerButton.y + profileDrawerButton.width
                }
                Text {
                    color: "#FFFFFF"
                    font.pixelSize: facade.doPx(18);
                    font.family: museoSansMidle.name
                    horizontalAlignment: Text.AlignHCenter
                    anchors {
                        horizontalCenter:parent.horizontalCenter
                    }

                    text: "Евразийская Неделя\nМаркетинга 2016";
                }

                Text {
                    color: "#FFFFFF"
                    font.pixelSize: facade.doPx(24);
                    font.family: museoSansMidle.name
                    horizontalAlignment: Text.AlignHCenter
                    anchors {
                        horizontalCenter:parent.horizontalCenter
                    }

                    text: event_handler.getLogin() + " " + event_handler.getFamily()
                }
            }
        }
    }

    ListView {
        id: listView
        width: parent.width
        anchors {
            bottom: parent.bottom
            topMargin: facade.toPx(200)
            top:profileScreenNavbarMenu.bottom
        }
        boundsBehavior: Flickable.StopAtBounds

        Component.onCompleted:{
            profileModel.clear()
            profileModel.append({
                image: "",
                target1:event_handler.getPost(),
                target2:event_handler.getCompany()
            });
            if(event_handler.getDescrip() != "")
            profileModel.append({
                image: "",
                target1: "",
                target2:event_handler.getDescrip()
            });
            profileModel.append({
                image:"ui/profiles/phoneIcon.png",
                target1: "Номер телефона",
                target2: event_handler.getPhone()
            });
            profileModel.append({
                image: "ui/profiles/mailIcon.png",
                target1: "E-mail",
                target2: event_handler.getMail()
            });
        }

        model:  ListModel {
            id: profileModel
                ListElement {
                    image: "";
                    target1: "";
                    target2: "";
                }
            }
        delegate: Rectangle {
            anchors {
                left: parent.left;
                right:parent.right
            }
            height: text1.implicitHeight + text2.implicitHeight + facade.toPx(80)
            Column {
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                Rectangle {
                    id: myrow
                    x: listView.width/2 - width/2
                    width: (event_handler.getDescrip() != "" && index==1)?
                            parent.width-facade.toPx(120):facade.toPx(600)
                    height: text1.implicitHeight + text2.implicitHeight + facade.toPx(40)
                    Item {
                        anchors.fill: parent
                        Image {
                            width: facade.toPx(sourceSize.width);
                            height:facade.toPx(sourceSize.height)
                            id: imageIcon
                            source: image
                        }
                        Column {
                            spacing: facade.toPx(5);
                            anchors {
                                left:imageIcon.right
                                right: parent.right;
                                leftMargin: (index == 0)?facade.toPx(10): facade.toPx(20)
                            }
                            Text {
                                id: text1
                                text: target1
                                color: "#7680B1"
                                width: parent.width;
                                wrapMode: Text.Wrap;
                                font {
                                    bold: true
                                    family: museoSansMidle.name
                                    pixelSize: (index>0)?facade.doPx(24): facade.doPx(26)
                                }
                                horizontalAlignment: Text.AlignHCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                            Text {
                                id: text2
                                text: target2
                                color: "#7680B1"
                                width: parent.width;
                                wrapMode: Text.Wrap;
                                font {
                                    family: museoSansMidle.name
                                    pixelSize: (index>0)?facade.doPx(26): facade.doPx(24)
                                }
                                horizontalAlignment: Text.AlignHCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }
                    }
                }
                Rectangle {// подчеркивание
                    height: 2
                    anchors {
                        left : parent.left;
                        right: parent.right
                    }
                    y: index * myrow.height
                    border.color: "#B8BDD6"
                }
            }
        }
    }

    Button {
        id: avatarButton
        width: facade.toPx(300)
        height:facade.toPx(300)
        x: parent.width/2 - bug.width/2
        anchors.topMargin:- bug.width/2

        onClicked: picDialogAndroid.visible = true

        anchors.top:profileScreenNavbarMenu.bottom

        background: Rectangle {
            id: bug
            clip: true
            smooth: true
            visible: false
            width: parent.width;
            height:parent.height
            anchors.verticalCenter: parent.verticalCenter;
            Image {
                id: bug2
                source: event_handler.getPicture()
                height:sourceSize.width>sourceSize.height?
                       parent.height:
                       sourceSize.height * (parent.width/sourceSize.width)
                width: sourceSize.width>sourceSize.height?
                       sourceSize.width*(parent.height/sourceSize.height):
                       parent.width
            }
        }

        Image {
            id: mask
            smooth: true;
            visible:false
            source: "ui/uimask/roundMask.png"
            sourceSize:
                Qt.size(facade.toPx(300), facade.toPx(300))
        }

        OpacityMask {
            source: bug
            maskSource: mask
            anchors.fill:bug
        }
    }

    /*
    FileDialog {
        id: fileDialog
        sidebarVisible: true;
        title:"Выберите фото"
        folder: shortcuts.pictures
        onAccepted: {
            visible = false;
            bug.source = fileDialog.fileUrls
            event_handler.setPicture(fileDialog.fileUrls)
            event_handler.sendAvatar(decodeURIComponent(fileDialog.fileUrls))
        }
        nameFilters:
            [ "Image files(*.jpg *.png)", "All files (*)" ]
    }
    */

    //--------------------------------------------------dialogPhotoAndroid-----------------------------------------
    Button {
        visible: false
        anchors.fill: parent
        id: picDialogAndroid
        text: "Установите себе фотографию на аватар"

        background: Rectangle {
            opacity: 0.8
            color: "#404040"
        }

        onClicked: visible = false;

        contentItem:Text{opacity:0}

        Rectangle {
            color: "#f7f7f7"
            radius: facade.toPx(25)
            y: parent.height/2-height/2
            x: parent.width /2-width /2
            width:2.2*picDialogAndroid.width/3
            height:picDialogAndroid.height/2.5

            // Область для сообщения диалогового окна
            Rectangle {
                color: "#f7f7f7"
                width: parent.width
                radius: facade.toPx(25)
                anchors.top: parent.top
                anchors.bottom: picDialogAndroidrow.top

                Label {
                    color:"#000000"
                    wrapMode: Text.Wrap
                    anchors.centerIn:parent
                    text: picDialogAndroid.text
                    width: parent.parent.width;
                    horizontalAlignment: Text.AlignHCenter
                    font {
                        pixelSize: facade.doPx(22)
                        family:museoSansLight.name
                    }
                }
            }

            Rectangle {
                width: parent.width
                height: picDialogAndroidButtonGallery.height/2
                anchors {
                    top:picDialogAndroidrow.top
                    topMargin: facade.toPx(102.4)//event_handler.itIsAndroid()==true? facade.toPx(102.4):1
                }
                color: picDialogAndroidButtonGallery.pressed==true? "#d7d7d7": "#f7f7f7"
            }

            Column {
                id: picDialogAndroidrow
                height: facade.toPx(202)//event_handler.itIsAndroid()? facade.toPx(202): facade.toPx(101);
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                Rectangle {
                    height: 1.2
                    color: "#808080"
                    width: parent.width
                    id: picDialogDividerHorizontal1
                    //visible: event_handler.itIsAndroid()
                }

                Button {
                    width: parent.width
                    height: facade.toPx(100)
                    id: picDialogAndroidButtonCamera
                    //visible: event_handler.itIsAndroid()

                    onClicked: {
                        picDialogAndroid.visible = false
                        imagePicker.item.takePhoto()
                    }

                    contentItem: Text {
                        color: "#34aadc"
                        text: qsTr("Сделать фотографию")
                        verticalAlignment : Text.AlignVCenter
                        horizontalAlignment:Text.AlignHCenter
                        font {
                            bold: true
                            pixelSize: facade.doPx(24)
                            family: museoSansMidle.name
                        }
                    }

                    background: Rectangle {
                        color: picDialogAndroidButtonCamera.pressed? "#d7d7d7": "#f7f7f7"
                    }
                }

                Rectangle {
                    height: 1.2
                    color: "#808080"
                    width: parent.width
                    id : picDialogDividerHorizontal2;
                }

                Button {
                    width: parent.width
                    height: facade.toPx(100)
                    id: picDialogAndroidButtonGallery

                    contentItem: Text {
                        color:"#34aadc"
                        text:qsTr("Загрузить из галереи")
                        verticalAlignment : Text.AlignVCenter
                        horizontalAlignment:Text.AlignHCenter
                        font {
                            bold: true
                            pixelSize: facade.doPx(24)
                            family: museoSansMidle.name
                        }
                    }

                    background: Rectangle {
                        radius: facade.toPx(25)
                        color: picDialogAndroidButtonGallery.pressed? "#d7d7d7": "#f7f7f7"
                    }

                    onClicked: {
                        picDialogAndroid.visible = false
                        imagePicker.item.pickImage()
                        /*
                        if (event_handler.itIsAndroid())
                            imagePicker.item.pickImage()
                        else fileDialog.open()
                        */
                    }
                }
            }

            RadialGradient {
                opacity: 0.2
                anchors.fill: parent
                gradient: Gradient {
                    GradientStop {
                        position: 0.0;
                        color: "white"
                    }
                    GradientStop {
                        position: 0.8;
                        color: "black"
                    }
                }
            }
        }
    }
    //-------------------------------------------------------------------------------------------------------------

    Loader {
        id: imagePicker
        source: event_handler.itIsAndroid()?
                "AndroidImagePicker.qml":
                "IOSImagePicker.qml"

        onLoaded: {
            item.onChange = function(url) {
                bug2.source = url
                event_handler.setPicture(url)
                event_handler.sendAvatar(decodeURIComponent(url))
            }
        }
    }

    EmwStyle.DialogWindow{
        id: dialogAndroid;
    }
}
