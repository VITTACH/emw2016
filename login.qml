import QtQuick 2.7
import QtQuick.Controls 2.0
import "EmwStyle" as EmwStyle

//--------------------------------------------------parent.qml---------------------------------------------
Item {
    Image {
            x:  parent.width / 2 - width / 2;
            y: (parent.width < parent.height)?
                parent.height/ 2 - height/ 2:0
        height:(parent.width < parent.height)?
                parent.height:
                (width/sourceSize.width)*sourceSize.height
        width: (parent.width < parent.height)?
                sourceSize.width*parent.height
                /sourceSize.height:
                parent.width
        source:"ui/background/background2.png"
    }

    Text {
        color: "#FFFFFF"
        font.pixelSize: facade.doPx(25);
        font.family: museoSansMidle.name
        horizontalAlignment: Text.AlignHCenter
        y: facade.toPx(20) + font.pixelSize/2;

        anchors {
            left: parent.left;
            right:parent.right
            leftMargin: parent.width/ 2 - facade.toPx(50);
            rightMargin:parent.width/ 2 - facade.toPx(50);
        }

        text: "Вход в приложение"
    }

    ListView {
        id: listView
        width: parent.width
        spacing: facade.toPx(50)
        anchors {
            top: parent.top
            bottom:parent.bottom
            topMargin:0.22*parent.height
        }
        boundsBehavior: Flickable.StopAtBounds

        model:  ListModel {
            id: registModel
                ListElement {
                    image: "ui/screensIcons/phoneIconWhite.png"
                    plaseholder: "Номер телефона";
                }
                ListElement {
                    image: "ui/screensIcons/passIconWhite.png"
                    plaseholder: "Введите ваш пароль";
                }
                ListElement {
                    image: ""
                    plaseholder: "Войти";
                }
                ListElement {
                    image: ""
                    plaseholder: "Еще нет аккаунта?";
                }
                ListElement {
                    image: "ui/loginlogo.png"
                    plaseholder: "";
                }
            }
        delegate: Column {
            width: parent.width
            height: index == 2?
                    facade.toPx(110):
                        (index == 4 ?
                             loginLogoImage.height: facade.toPx(89))
            Button {
                text: plaseholder
                height: facade.toPx(110)
                visible: index == 2? 1:0
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    leftMargin: 0.09 * parent.width;
                    rightMargin:0.09 * parent.width;
                }

                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name

                onClicked: {
                    if (loader.fields[0].length <11) {
                        dialogAndroid.text = "Укажите корректный номер"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[1].length < 5) {
                        dialogAndroid.text = "Пароль короче 5 символов"
                        dialogAndroid.visible = true
                    }
                    else {
                        busyIndicator.visible = true
                        switch(event_handler.networkLogin(
                                   loader.fields[0],
                                   loader.fields[1])
                               )
                        {
                        case 1:
                            loader.goTo("qrc:/profile.qml")
                            break;
                        case 0:
                            dialogAndroid.text = "Вы не зарегистрированы!"
                            dialogAndroid.visible=true
                            break;
                        case -1:
                            dialogAndroid.text = "Нет доступа к интернету!"
                            dialogAndroid.visible=true
                            break;
                        }
                        busyIndicator.visible = false;
                    }
                }
                background: Rectangle {
                    radius:facade.toPx(25)
                    color: parent.down?"#51587F":"#7680B1"
                }
                contentItem: Text {
                    color: parent.down?"#FFFFFF":"#FFFFFF"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment : Text.AlignVCenter
                    opacity: enabled?1:0.3
                    elide: Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }

            Button {
                text: plaseholder
                visible: index == 3? 1:0
                id: mainScreenLabelForgotPass

                background: Rectangle{opacity:0}

                font.pixelSize: facade.doPx(22);
                font.family: museoSansMidle.name

                //font.underline: true
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }

                onClicked: loader.goTo("qrc:/regist.qml")

                contentItem: Text {
                    color:mainScreenLabelForgotPass.down? "#960f133d": "#FFFFFF"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment : Text.AlignVCenter
                    text: mainScreenLabelForgotPass.text;
                    font: mainScreenLabelForgotPass.font;
                    opacity: enabled?1:0.3
                    elide: Text.ElideRight
                    padding: -8
                }
            }

            Image {
                id: loginLogoImage
                visible: (index == 4)? 1:0;
                x: parent.width/2 -width/2;
                width: (parent.width<1024)? sourceSize.width / 2 * parent.width/ 500:sourceSize.width / 2 * 2.048
                height:(parent.width<1024)? sourceSize.height/ 2 * parent.width/ 500:sourceSize.height/ 2 * 2.048
                source: image
            }

            Row {
                height: facade.toPx(88)
                visible:index < 2? 1:0;
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
                Image {
                    source: image
                    width: facade.toPx(sourceSize.width * 1.5)
                    height:facade.toPx(sourceSize.height* 1.5)
                }
                TextField {
                    id: textFieldInRow
                    height: facade.toPx(88)
                    anchors {
                        left: parent.left
                        right: parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(20)
                    }
                    placeholderText: plaseholder

                    onTextChanged: loader.fields[index] = text

                    color: "#FFFFFF"
                    echoMode: (index == 1)?
                    (TextInput.Password):TextInput.Normal

                    inputMethodHints: Qt.ImhFormattedNumbersOnly

                    onFocusChanged: {
                        if(text.length== 0 && index == 0)
                        text = "+7"
                    }

                    background: Rectangle {opacity:0}
                    font.family: museoSansMidle.name
                    font.pixelSize: facade.doPx(28)
                }
            }

            Rectangle {//ебаное нижне подчеркивание
                height: 1
                color: "#FFFFFF"
                visible: index < 2?1:0
                anchors {
                    left: parent.left;
                    right:parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
            }
        }
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }
}
//-------------------------------------------------------------------------------------------------------------
