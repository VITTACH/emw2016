QT += qml quick quickcontrols2 network widgets websockets

CONFIG += c++11 qml_debug

android {
        QT += androidextras
        ANDROID_PACKAGE_SOURCE_DIR= $$PWD/android-sources
        include($$PWD/vendor/com/github/benlau/quickandroid/quickandroid.pri)
        # Additional import path used to resolve QML modules in Qt Creator' s
        QML_IMPORT_PATH += $$PWD/vendor/com/github/benlau/quickandroid/
}

ios {
        include($$PWD/vendor/com/github/benlau/quickios-master/quickios.pri)
        # Additional import path used to resolve QML modules in Qt Creator' s
        QML_IMPORT_PATH += $$PWD/vendor/com/github/benlau/quickios-master/
}

SOURCES += main.cpp \
    eventhandler.cpp \
    user.cpp \
    httpnetwork.cpp \
    colorimageprovider.cpp \
    wrapper.cpp

RESOURCES += \
    qml.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    eventhandler.h \
    user.h \
    httpnetwork.h \
    colorimageprovider.h \
    wrapper.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
