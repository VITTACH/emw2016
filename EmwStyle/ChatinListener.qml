import QtQuick 2.0

Item {
    Timer {
        repeat: true
        running: true
        interval: 100
        Component.onCompleted: {
            event_handler.getMessage(event_handler.getMemberIndex(), -1)
        }
        onTriggered:{
            if (event_handler.getMemberIndex()!=loader.currentChatIndex)
            {
                dialogAndroid.text="Вам поступило сообщение. Прочитать?"
                dialogAndroid.visible = true
            }
        }
    }
}
