import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

//--------------------------------------------------dialogAndroid----------------------------------------------
Button {
    z: 3
    visible: false
    id: dialogAndroid
    anchors.fill: parent

    background: Rectangle {
        opacity: 0.8
        color: "#404040"
    }

    contentItem:Text{opacity:0}

    Rectangle {
        color: "#f7f7f7"
        radius: facade.toPx(25)
        y: parent.height/ 2 -height / 2
        x: parent.width / 2 - width / 2
        height:dialogAndroid.height/2.5
        width:2.2*dialogAndroid.width/3

        //место отобажения сообщения диалогового окна
        Rectangle {
            color: "#f7f7f7"
            radius: facade.toPx(25)

            Label {
                color:"#000000"
                wrapMode: Text.Wrap
                text: dialogAndroid.text;
                anchors.centerIn: parent;
                width:parent.parent.width
                horizontalAlignment: Text.AlignHCenter
                font {
                    pixelSize: facade.doPx(22)
                    family:museoSansLight.name
                }
            }

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                bottom: dialogAndroidDividerHorizontal.top
            }
        }

        // Создаём горизонтальный разделитель
        Rectangle {
            height: 1
            color: "#808080"
            anchors {
                left: parent.left
                right: parent.right
                bottom: dialogAndroidrow.top
            }
            id: dialogAndroidDividerHorizontal
        }

        Rectangle {
            height: facade.toPx(25)
            color: dialogAndroidButtonOk.pressed? "#d7d7d7": "#f7f7f7"
            width: dialogAndroid.text == newMessage?
                       parent.width/2-0: parent.width;
            anchors {
                right: parent.right
                top: dialogAndroidDividerHorizontal.bottom
            }
        }

        Rectangle {
            anchors {
                left: parent.left
                top: dialogAndroidDividerHorizontal.bottom
            }
            width: parent.width / 2;
            height: facade.toPx(25);
            color:dialogAndroidButtonCancel.pressed==true?
                      "#d7d7d7": "#f7f7f7"
            visible:(dialogAndroid.text == newMessage)?1:0
        }

        Row {
            id: dialogAndroidrow
            // А также прибиваем строку к низу у диалогового окна
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            height: dialogAndroid.height< 900?
                        facade.toPx(100): 100;

            Button {
                width: parent.width/2-1;
                anchors.top: parent.top;
                anchors.bottom: parent.bottom

                id: dialogAndroidButtonCancel
                visible: (dialogAndroid.text == newMessage)? 1: 0

                contentItem: Text {
                    color: "#34aadc"
                    text: qsTr("Отмена")
                    verticalAlignment: Text.AlignVCenter;
                    horizontalAlignment:Text.AlignHCenter
                    font {
                        pixelSize: facade.doPx(24);
                        family: museoSansMidle.name
                    }
                }

                background: Rectangle {
                    radius: facade.toPx(25)
                    color: dialogAndroidButtonCancel.pressed==true?
                               "#d7d7d7": "#f7f7f7"
                }

                onClicked: {
                    loader.currentChatIndex = event_handler.getMemberIndex()
                    event_handler.setInChat(true)
                    dialogAndroid.visible = false
                    dialogAndroid.text= ""
                }
            }

            //Создаю разделитель между кнопками шириной в 2 пикселя
            Rectangle {
                color: "#808080"
                width: facade.toPx(2)
                visible: (dialogAndroid.text == newMessage) ? 1: 0;
                // Растягиваем разделитель по высоте объекта строки
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                }
            }

            Button {
                id: dialogAndroidButtonOk
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                }
                width:(dialogAndroid.text == newMessage)?
                           parent.width/2-1: parent.width

                contentItem: Text {
                    color: "#34aadc"
                    text: dialogAndroid.text=="Благодарим\n\nВаша заявка уже принята в работу.\nМы свяжемся с вами в самое ближайшее время."?qsTr("Спасибо"):qsTr("Хорошо")
                    verticalAlignment: Text.AlignVCenter;
                    horizontalAlignment:Text.AlignHCenter
                    font {
                        bold: true;
                        pixelSize: facade.doPx(24);
                        family: museoSansMidle.name
                    }
                }

                background: Rectangle {
                    radius: facade.toPx(25)
                    color: dialogAndroidButtonOk.pressed? "#d7d7d7":"#f7f7f7"
                }

                onClicked: {
                    dialogAndroid.visible = false
                    if(dialogAndroid.text == "Успешно зарегистрированы"
                    || dialogAndroid.text == "Изменения были приняты"){
                        loader.drawerCurrentIndex = 0;
                        loader.goTo("qrc:/profile.qml")
                    }
                    if(dialogAndroid.text==newMessage){
                        event_handler.curMemberIndex(loader.currentChatIndex)
                        loader.goTo("qrc:/chat.qml")
                        loader.reloadChat = true;
                    }
                    if(dialogAndroid.text == "Разработчик VITTACH\nСпециально для AppLabs LLC.\n\nСпасибо за использование нашего приложения")
                        if(event_handler.itIsAndroid())
                        Qt.openUrlExternally("http://vk.com/vittach")
                    dialogAndroid.text= ""
                }
            }
        }

        RadialGradient {
            opacity: 0.2
            anchors.fill: parent
            gradient: Gradient {
                GradientStop {
                    position: 0.0;
                    color: "white"
                }
                GradientStop {
                    position: 0.8;
                    color: "black"
                }
            }
        }
    }

    property string newMessage: "Вам поступило сообщение. Прочитать?"
}
//-------------------------------------------------------------------------------------------------------------
