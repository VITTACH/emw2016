import QtQuick 2.7
import QtQuick.Controls 2.0

Drawer {
    id: drawer
    focus: false
    Rectangle {
        color:"#7680B1"
        anchors.fill: parent
    }

    property int work: 1
    height:parent.height
    width: 0.75 * parent.width
    dragMargin:facade.toPx(20)

    Rectangle {
        clip: true
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: facade.toPx(10)
            top: navigateDrawerLogoImage.bottom
        }

        ListView {
            id: listView
            anchors {
                fill: parent
                topMargin: facade.toPx(30)
            }
            boundsBehavior: Flickable.StopAtBounds
            Component.onCompleted: {
                currentIndex = loader.drawerCurrentIndex
            }

            model:  ListModel {
                    ListElement {
                        image: "qrc:/ui/navbarIcons/profile.png";
                        target: "Профиль";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/mailButton.png";
                        target: "Сообщения";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/eventProgram.png";
                        target: "Программа мероприятия";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/members.png";
                        target: "Участники";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/events.png";
                        target: "Грядущие мероприятия";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/info.png";
                        target: "Контактная информация";
                    }
                    ListElement {
                        image: "qrc:/ui/navbarIcons/developers.png";
                        target: "О разработчике";
                    }
                    ListElement {image: ""; target: "";}
                    ListElement {
                        image: "qrc:/ui/navbarIcons/exit.png";
                        target: "Выход";
                    }
                }
            delegate: Component {
                Rectangle {
                    width: parent.width;
                    height: facade.toPx(90)
                    color: ListView.isCurrentItem? "lightgrey": "#FFFFFF";
                    MouseArea {
                        anchors.fill: parent;
                        propagateComposedEvents: true
                        onPressed: {
                            if(index < 7 || index == 8)
                            listView.currentIndex=index
                        }
                        onReleased:{
                            listView.currentIndex = -1;
                        }

                        onClicked: {
                            if(index < 7) {
                                drawer.close();
                                listView.currentIndex = index
                                loader.drawerCurrentIndex = index
                            }
                            switch(index) {
                            case 0: loader.goTo("qrc:/profile.qml"); break
                            case 1: loader.goTo("qrc:/dialogs.qml"); break
                            case 2: loader.goTo("qrc:/speaker.qml"); break
                            case 3: loader.goTo("qrc:/members.qml"); break
                            case 4: loader.goTo("qrc:/event.qml"); break
                            case 5: loader.goTo("qrc:/contact.qml"); break
                            case 6: loader.goTo("qrc:/develop.qml"); break
                            case 8:
                                event_handler.exitMenu()
                                loader.goTo("qrc:/login.qml")
                                break;
                            }
                        }
                    }
                    Row {
                        anchors{
                            fill: parent
                            leftMargin: facade.toPx(20)
                        }
                        spacing: facade.toPx(25)
                        Image {
                            source:image;
                            width: facade.toPx(sourceSize.width *1.5)
                            height:facade.toPx(sourceSize.height*1.5)
                            anchors.verticalCenter: parent.verticalCenter;
                        }
                        Text {
                            text: target;
                            color: "#51587F"
                            // wrapMode: Text.Wrap
                            font.pixelSize: facade.doPx(19);
                            font.family: museoSansMidle.name
                            width: (target == "Программа мероприятия")?
                                       0.75 * drawer.width:
                                       ((target=="Контактная информация")?
                                       0.75 * drawer.width-facade.toPx(7):
                                       0.75 * drawer.width-facade.toPx(24)
                                       )
                            anchors.verticalCenter: parent.verticalCenter;
                        }
                    }
                }
            }
        }
    }

    Image {
        anchors.top: parent.top
        id: navigateDrawerLogoImage
        anchors.topMargin: facade.toPx(10);
        x: facade.toPx(20)
        y: listView.model.get(0).y-height/2
        source: "qrc:/ui/navbarMenu/navbarLogo.png"
        width: (parent.height > 900)?
                sourceSize.width /1.5*(parent.height/900):
                sourceSize.width /1.5
        height:(parent.height > 900)?
                sourceSize.height/1.5*(parent.height/900):
                sourceSize.height/1.5
    }
}
