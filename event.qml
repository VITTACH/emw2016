import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootEvent
    //property int pageindex: 0

    EmwStyle.NavigateDrawer
    {id: eventScreenDrawer;}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Timer {
        id: upload
        running: true
        interval: 500
        onTriggered:{
            event_handler.getEvents(/*pageindex*/)
            var site;
            var desc;
            var avatar;
            while(((avatar = event_handler.takeEvent(0)) != "FATAL_ERROR")
                  &&((desc = event_handler.takeEvent(1)) != "FATAL_ERROR")
                  &&((site = event_handler.takeEvent(2)) != "FATAL_ERROR")
                  ) {
                    eventsModel.append({
                        image: avatar,
                            target0: desc,
                                target1: site
                    });
                    event_handler.takeEvent(-1)
            }
            busyIndicator.visible = false

            /*
            showMoreEventsButton.visible= event_handler.isManyEvents()?1:0
            showMoreEventsButton.height = event_handler.isManyEvents()?
                facade.toPx(15) + showMoreEventsButton.implicitHeight: 0
            if(pageindex > 0)
            listView.positionViewAtEnd();
            */
        }
    }

    ListView {
        Component.onCompleted: {
            eventsModel.clear();
            busyIndicator.visible = true;
        }

        id: listView
        width: parent.width
        boundsBehavior:Flickable.StopAtBounds
        maximumFlickVelocity: 0
        anchors{
            bottom: parent.bottom
            top: eventScreenNavbarMenu.bottom
            /*
            topMargin: facade.toPx(-30)
            top : showMoreEventsButton.bottom
            */
        }

        model:  ListModel {
                id:eventsModel
                ListElement {
                    image: "";
                    target0: "";
                    target1: "";
                    target2: "";
                    //target3: "";
                }
            }
        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: Math.max(bug.height,
                                 text1.implicitHeight +
                                 text2.implicitHeight
                             ) + facade.toPx(60)
            LinearGradient {
                anchors.fill: parent
                start:Qt.point(0, 0)
                end : Qt.point(0, facade.toPx(30))
                gradient: Gradient {
                    GradientStop {
                        position: 1.0; color: "white"
                    }
                    GradientStop {
                        position: 0; color: "#E5E5E5"
                    }
                }
            }
            Column {
                width: parent.width
                anchors.bottom: parent.bottom
                Rectangle {
                    id: myrow
                    x:listView.width / 2 - width / 2

                    MouseArea {
                        anchors.fill: parent
                        onPressed: {
                            myrow.color = "#E5E5E5";
                        }
                        onReleased: {
                            myrow.color = "#FFFFFF";
                        }
                        onPositionChanged: {
                            myrow.color = "#FFFFFF";
                        }
                        onClicked:Qt.openUrlExternally("http://"+target1)
                    }

                    height:Math.max(bug.height,
                                    text1.implicitHeight +
                                    text2.implicitHeight
                                    ) + facade.toPx(20)
                    width: parent.width-facade.toPx(10)

                    Item {
                        anchors.fill: parent
                        anchors.leftMargin: facade.toPx(20)
                        Rectangle {
                            id: bug
                            clip: true
                            smooth: true
                            visible: false
                            width: facade.toPx(200)
                            height:facade.toPx(200)
                            anchors {
                                top: parent.top
                                topMargin: facade.toPx(20)
                            }
                            Image {
                                source: image
                                height:sourceSize.width>sourceSize.height?
                                       facade.toPx(200):
                                       sourceSize.height * (parent.width/sourceSize.width)
                                width: sourceSize.width>sourceSize.height?
                                       sourceSize.width*(parent.height/sourceSize.height):
                                       facade.toPx(200)
                            }
                        }

                        Image {
                            id: mask
                            smooth: true
                            visible: false
                            source: "ui/uimask/roundMask.png"
                            sourceSize:
                              Qt.size(facade.toPx(300), facade.toPx(300))
                        }

                        OpacityMask {
                            source: bug
                            maskSource: mask
                            anchors.fill: bug
                        }
                        Column {
                            width: parent.width/1.5
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                            Text {
                                id: text1
                                color: "#BABABB"
                                wrapMode: Text.Wrap
                                //horizontalAlignment:Text.AlignJustify
                                width: parent.width - facade.toPx(20)
                                font.pixelSize: facade.doPx(24)
                                font.family:museoSansMidle.name
                                text: target0;
                            }
                            Text {
                                id: text2
                                color: "#5E5E5E"
                                wrapMode: Text.Wrap
                                width: parent.width - facade.toPx(20)
                                font.pixelSize: facade.doPx(24)
                                font.family:museoSansMidle.name
                                text:"\nПодробнее на: "+target1
                            }
                        }
                    }
                }
                Rectangle {// подчеркивание
                    height: 2
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    y: index * myrow.height
                    border.color: "#B8BDD6"
                }
            }
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(140);
        id: eventScreenNavbarMenu

        Image {
            x: rootEvent.width/2- width/2
            y: -(height-facade.toPx(280))
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootEvent.width < rootEvent.height / 1.5)?
                    rootEvent.height / 2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootEvent.width < rootEvent.height / 1.5)?
                    sourceSize.width *(rootEvent.height/2/sourceSize.height):
                    rootEvent.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: eventDrawerButton;
            width:2*hambrButtonImage.width
            height:hambrButtonImage.height
            background:Rectangle{opacity:0}

            onClicked: {eventScreenDrawer.open()}

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Column {
            anchors {
                centerIn: parent
                leftMargin: eventDrawerButton.y + eventDrawerButton.width
            }
            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("EMW 2016")
            }

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("Мероприятия")
            }
        }
    }

    /*
    Button {
        width: parent.width
        id: showMoreEventsButton
        text: qsTr("Показать еще")
        anchors.top:eventScreenNavbarMenu.bottom

        font.underline: true
        font.pixelSize: facade.doPx(23);
        font.family: museoSansMidle.name

        background: Rectangle {color: "#E5E5E5"}

        onClicked: {
            pageindex = pageindex + 1
            busyIndicator.visible = true
            upload.restart()
        }

        contentItem: Text {
            color: "#0f133d"
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            text: parent.text
            font: parent.font
            padding: -8
        }
    }
    */

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }
}
