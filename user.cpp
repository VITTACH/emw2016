#include "user.h"

using namespace std;

int User::getId() {
    return id;
}

int User::getStatus() {
    return status;
}

int User::getGender() {
    return gender;
}

string User::getSsid(){
    return ssid;
}

string User::getPost(){
    return post;
}

string User::getMail(){
    return mail;
}

string User::getCity(){
    return city;
}

string User::getTime(){
    return time;
}

string User::getTheme() {
    return theme;
}

string User::getNumber() {
    return number;
}

string User::getCompany() {
    return company;
}

void User::setId(int sid) {
    this->id = sid;
}

string User::getPicture() {
    return picture;
}

string User::getDescrip() {
    return descript;
}

string User::getUserName(){
    return userName;
}

string User::getLastName(){
    return lastName;
}

string User::getResponse(){
    return response;
}

string User::getPassword(){
    return password;
}

void User::setCity(string city) {
    this->city = city;
}

void User::setSsid(string ssid) {
    this->ssid = ssid;
}

void User::setPost(string post) {
    this->post = post;
}

void User::setTime(string time) {
    this->time = time;
}

void User::setMail(string email) {
    this->mail = email;
}

void User::setStatus(int status) {
    this->status = status;
}

void User::setGender(int gender) {
    this->gender = gender;
}

void User::setTheme(string theme) {
    this->theme = theme;
}

void User::setDescrip(string des) {
    this->descript= des;
}

void User::setMessage(string message, bool flag) {
    msg msgStruct;
    msgStruct.flag=!flag;
    msgStruct.message=message;
    this->message.push_back(msgStruct);
    if (flag == true) currentMessage++;
}

void User::setNumber(string number) {
    this->number = number;
}

void User::setPicture(string picture) {
    this->picture = picture;
}

void User::setCompany(string company) {
    this->company = company;
}

void User::setUserName(string userName) {
    this->userName = userName;
}

void User::setLastName(string lastName) {
    this->lastName = lastName;
}

void User::setPassword(string password) {
    this->password = password;
}

void User::setResponse(string response) {
    this->response=response;
}

bool User::getMessageFlag() {
    return message[currentMessage-1].flag;
}

bool User::getEndMessageFlag() {
    if(message.size()>0)
    return message[message.size()-1].flag;
    return true;
}

string User::getEndMessage(){
    if(message.size()>0)
    return message[message.size()-1].message;
    return "";
}

string User::getMessage(int index) {
    if (currentMessage < message.size()
        && index>=0){
        if(index>0) {
        string s;
        s= message[currentMessage++].message;
        return s;
        }
        else return message[0].message;
    }
    if(index<0)
    currentMessage=0;
    return "";
}

User::User() {

}
