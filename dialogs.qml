import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootEvent
    //property int pageindex: 0

    EmwStyle.NavigateDrawer
    {id: eventScreenDrawer;}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Timer {
        id: upload
        repeat: true
        running: true
        interval: 500
        onTriggered:{
            if (loader.reloadDialogs) {
                loader.reloadDialogs = false;

                dialogsModel.clear();
                event_handler.takeDialog(-2);
                var readed;
                var author;
                var avatar;
                var company;
                var message;
                var fulName;
                while(((avatar=event_handler.takeDialog(0))!= "FATAL_ERROR")
                      &&((fulName=event_handler.takeDialog(1))!= "FATAL_ERROR")
                      &&((company=event_handler.takeDialog(2))!= "FATAL_ERROR")
                      &&((message=event_handler.takeDialog(3))!= "FATAL_ERROR")
                      &&((readed =event_handler.takeDialog(4))!= "FATAL_ERROR")
                      &&((author =event_handler.takeDialog(5))!= "FATAL_ERROR")
                      ) {
                        dialogsModel.append({
                            image: avatar,
                                target0: fulName,
                                    target1: company,
                                        target2: message.replace(/(\r\n|\n|\r)/gm,""),
                                            target3: readed,
                                                target4: author
                        });
                        event_handler.takeDialog(-1)
                }
                busyIndicator.visible = false

                /*
                showMoreEventsButton.visible= event_handler.isManyEvents()?1:0
                showMoreEventsButton.height = event_handler.isManyEvents()?
                    facade.toPx(15) + showMoreEventsButton.implicitHeight: 0
                if(pageindex > 0)
                listView.positionViewAtEnd();
                */
            }
        }
    }

    ListView {
        Component.onCompleted: {
            loader.reloadDialogs = true
            busyIndicator.visible= true
        }

        id: listView
        width: parent.width
        boundsBehavior:Flickable.StopAtBounds
        maximumFlickVelocity: 0
        anchors{
            bottom: parent.bottom
            top: eventScreenNavbarMenu.bottom
            /*
            topMargin: facade.toPx(-30)
            top : showMoreEventsButton.bottom
            */
        }

        model:  ListModel {
                id:dialogsModel
                ListElement {
                    image: "";
                    target0: "";
                    target1: "";
                    target2: "";
                    target3: "";
                    target4: "";
                }
            }
        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: Math.max(bug.height+facade.toPx(20),
                                 text1.implicitHeight +
                                 text2.implicitHeight +
                                 text3.implicitHeight
                             ) + facade.toPx(100)
            LinearGradient {
                anchors.fill: parent
                start:Qt.point(0, 0)
                end : Qt.point(0, facade.toPx(30))
                gradient: Gradient {
                    GradientStop {
                        position: 1.0; color: "white"
                    }
                    GradientStop {
                        position: 0; color: "#E5E5E5"
                    }
                }
            }
            Column {
                width: parent.width
                anchors.bottom: parent.bottom
                Rectangle {
                    id: myrow
                    x:listView.width / 2 - width / 2

                    MouseArea {
                        anchors.fill: parent
                        onPressed: {
                            myrow.color = "#E5E5E5";
                        }
                        onReleased: {
                            myrow.color = "#FFFFFF";
                        }
                        onPositionChanged: {
                            myrow.color = "#FFFFFF";
                        }
                        onClicked: {
                            var pos;
                            pos=event_handler.getDialog(index)
                            event_handler.curMemberIndex(pos);
                            event_handler.readMessage(index);
                            loader.currentChatIndex=pos;
                            loader.goTo("qrc:/chat.qml")
                            loader.chatVariable = 0;
                        }
                    }

                    height:Math.max(bug.height+facade.toPx(20),
                                    text1.implicitHeight +
                                    text2.implicitHeight +
                                    text3.implicitHeight
                                    ) + facade.toPx(60)
                    width: parent.width-facade.toPx(10)

                    Item {
                        anchors.fill: parent
                        anchors.leftMargin: facade.toPx(20)
                        Rectangle {
                            id: bug
                            clip: true
                            smooth: true
                            visible: false
                            width: facade.toPx(200)
                            height:facade.toPx(200)
                            anchors {
                                top: parent.top
                                topMargin: facade.toPx(20)
                            }
                            Image {
                                source: image
                                height:sourceSize.width>sourceSize.height?
                                       facade.toPx(200):
                                       sourceSize.height * (parent.width/sourceSize.width)
                                width: sourceSize.width>sourceSize.height?
                                       sourceSize.width*(parent.height/sourceSize.height):
                                       facade.toPx(200)
                            }
                        }

                        Image {
                            id: mask
                            smooth: true
                            visible: false
                            source: "ui/uimask/roundMask.png"
                            sourceSize:
                              Qt.size(facade.toPx(300), facade.toPx(300))
                        }

                        OpacityMask {
                            source: bug
                            maskSource: mask
                            anchors.fill: bug
                        }
                        Column {
                            spacing:facade.toPx(20)
                            width: parent.width/1.5
                            anchors {
                                right: parent.right
                                verticalCenter: parent.verticalCenter
                            }
                            Text {
                                id: text1
                                color: "#2D2D2D"
                                wrapMode: Text.Wrap
                                //horizontalAlignment:Text.AlignJustify
                                width: parent.width - facade.toPx(20)
                                font.pixelSize: facade.doPx(24)
                                font.family:museoSansMidle.name
                                text: target0;
                            }
                            Text {
                                id: text2
                                color: "#B5B5B5"
                                wrapMode: Text.Wrap
                                width: parent.width - facade.toPx(20)
                                font.pixelSize: facade.doPx(20)
                                font.family:museoSansMidle.name
                                text: target1;
                            }
                            Row {
                                spacing: facade.toPx(20)
                                height: text3.implicitHeight
                                width: parent.width - facade.toPx(20)
                                Rectangle {
                                    visible: target3=="0"? 1: 0
                                    color: "#7680B1"
                                    radius: width/2;
                                    width: facade.toPx(15)
                                    height:facade.toPx(15)
                                    anchors.verticalCenter: parent.verticalCenter
                                }

                                Text {
                                    id: text3
                                    color: "#474747"
                                    width: parent.width
                                    elide: Text.ElideRight
                                    font.pixelSize: facade.doPx(20)
                                    font.family:museoSansMidle.name
                                    text: (target4=="1"?"Вам: ":"Вы: ")
                                          + target2.substr(0, 50);
                                }
                            }
                        }
                    }
                }
                Rectangle {// подчеркивание
                    height: 2
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    y: index * myrow.height
                    border.color: "#B8BDD6"
                }
            }
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(140);
        id: eventScreenNavbarMenu

        Image {
            x: rootEvent.width/2- width/2
            y: -(height-facade.toPx(280))
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootEvent.width < rootEvent.height / 1.5)?
                    rootEvent.height / 2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootEvent.width < rootEvent.height / 1.5)?
                    sourceSize.width *(rootEvent.height/2/sourceSize.height):
                    rootEvent.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: eventDrawerButton;
            width:2*hambrButtonImage.width;
            height:hambrButtonImage.height;
            background:Rectangle{opacity:0}

            onClicked: {eventScreenDrawer.open()}

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Column {
            anchors {
                centerIn: parent
                leftMargin: profileDrawerButton.y + profileDrawerButton.width
            }
            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("EMW 2016")
            }

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("Диалоги")
            }
        }
    }

    /*
    Button {
        width: parent.width
        id: showMoreEventsButton
        text: qsTr("Показать еще")
        anchors.top:eventScreenNavbarMenu.bottom

        font.underline: true
        font.pixelSize: facade.doPx(23);
        font.family: museoSansMidle.name

        background: Rectangle {color: "#E5E5E5"}

        onClicked: {
            pageindex = pageindex + 1
            busyIndicator.visible = true
            upload.restart()
        }

        contentItem: Text {
            color: "#0f133d"
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            text: parent.text
            font: parent.font
            padding: -8
        }
    }
    */

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }
}
