import QtQuick 2.7
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.0
import "EmwStyle" as EmwStyle

//--------------------------------------------------registScreen.qml-------------------------------------------
Item {
    ComboBox {
        visible: false

        model:ListModel {
            id:citysModel
            ListElement {
                text : ""
            }
        }
    }

    Timer {
        running:true
        interval: 10
        onTriggered: {
            event_handler.getCitys()
            var city;
            while((city = event_handler.takeCity()) != "FATAL_ERROR")
            citysModel.append({
            text: city
            });
        }
    }

    Image {
            x:  parent.width / 2 - width / 2;
            y: (parent.width < parent.height)?
                parent.height/ 2 - height/ 2:0
        height:(parent.width < parent.height)?
                parent.height:
                (width/sourceSize.width)*sourceSize.height
        width: (parent.width < parent.height)?
                sourceSize.width*parent.height
                /sourceSize.height:
                parent.width
        source: "ui/background/background4.png";
    }

    Text {
        color: "#FFFFFF"
        font.pixelSize: facade.doPx(25);
        font.family: museoSansMidle.name
        horizontalAlignment: Text.AlignHCenter

        y: mainScreenBackButton.y + mainScreenBackButton.height/2 - font.pixelSize/2 -facade.toPx(7)
        anchors {
            left: parent.left;
            right:parent.right
            leftMargin: parent.width /2 - facade.toPx(60)
            rightMargin: parent.width/2 - facade.toPx(60)
        }

        text: "Регистрация"
    }

    ListView {
        id: listView
        width: parent.width
        spacing: facade.toPx(50)
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: facade.toPx(140)
        }
        boundsBehavior: Flickable.StopAtBounds

        model:  ListModel {
            id: registModel
                ListElement {
                    image: "ui/screensIcons/humanIconWhite.png"
                    plaseholder: "Имя";
                }
                ListElement {
                    image: "ui/screensIcons/humanIconWhite.png"
                    plaseholder: "Фамилия";
                }
                ListElement {
                    image: "ui/screensIcons/homeIconWhite.png"
                    plaseholder: "Ваш город";
                }
                ListElement {
                    image: "ui/screensIcons/phoneIconWhite.png"
                    plaseholder: "E-mail";
                }
                ListElement {
                    image: "ui/screensIcons/badgeIconWhite.png"
                    plaseholder: "Должность";
                }
                ListElement {
                    image: "ui/screensIcons/companyIconWhite.png"
                    plaseholder: "Компания";
                }
                ListElement {
                    image: "ui/screensIcons/phoneIconWhite.png"
                    plaseholder: "Номер мобильный";
                }
                ListElement {
                    image: ""
                    plaseholder: "";
                }
                ListElement {
                    image: ""
                    plaseholder: "Зарегистрироваться";
                }
            }
        delegate: Column {
            width: parent.width
            height: index == 8?
                    facade.toPx(160):facade.toPx(89)
            Button {
                text: plaseholder
                height: facade.toPx(110)
                visible: index == 8? 1:0
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    leftMargin: 0.09 * parent.width;
                    rightMargin:0.09 * parent.width;
                }

                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name

                onClicked: {
                    if (loader.fields[0].length < 2) {
                        dialogAndroid.text = "Имя короче 2 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[1].length < 2) {
                        dialogAndroid.text = "Фамилия менее 2 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[6].length <11) {
                        dialogAndroid.text = "Укажите корректный номер"
                        dialogAndroid.visible = true
                    }
                    else {
                        busyIndicator.visible = true
                        switch(event_handler.registration(loader.fields[0],
                                                      loader.fields[1],
                                                      loader.fields[4],
                                                      loader.fields[5],
                                                      loader.fields[3],
                                                      loader.fields[6],
                                                      loader.fields[2],
                                                      loader.fields[7]=="1"? 1: 0)
                                )
                        {
                        case 1:
                            event_handler.setPost(loader.fields[4])
                            event_handler.setMail(loader.fields[3])
                            event_handler.setCity(loader.fields[2])
                            event_handler.setFamily(loader.fields[1])
                            event_handler.setCompany(loader.fields[5])
                            event_handler.setGender(loader.fields[7] == "1"? 1: 0)
                            event_handler.setPhone(loader.fields[6])
                            event_handler.setLogin(loader.fields[0])
                            dialogAndroid.text = "Успешно зарегистрированы"
                            dialogAndroid.visible=true
                            break;
                        case 0:
                            dialogAndroid.text = "Вы уже зарегистрированы!"
                            dialogAndroid.visible=true
                            break;
                        case -1:
                            dialogAndroid.text = "Нет доступа к интернету!"
                            dialogAndroid.visible=true
                            break;
                        }
                        busyIndicator.visible = false;
                    }
                }
                background: Rectangle {
                    radius:facade.toPx(25)
                    color:parent.down?"#51587F":"#7680B1"
                }
                contentItem: Text {
                    color:parent.down?"#FFFFFF":"#FFFFFF"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment : Text.AlignVCenter
                    opacity: enabled?1:0.3
                    elide: Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }

            Switch {
                leftPadding: 0
                height: facade.toPx(80)
                spacing:facade.toPx(15)
                visible:index == 7?1:0;
                id:registScreenSwitchGender

                font {
                pixelSize: facade.toPx(28);
                family: museoSansMidle.name
                }
                anchors {
                    left: parent.left
                    leftMargin: 0.09*parent.width;
                }
                onClicked: {
                    if(checked)loader.fields[7]="1"
                    else loader.fields[7] = "0"
                }
                text: checked? qsTr("Женщина"): qsTr("Мужчина");

                indicator: Rectangle {
                    radius:facade.toPx(30)
                    x: parent.leftPadding;
                    y: parent.height/2 - height/2;
                    implicitWidth:facade.toPx(120)
                    implicitHeight:facade.toPx(54)
                    color:parent.checked?"#760f133d":"#86FFFFFF"

                    Rectangle {
                        width: parent.parent.height/1.8
                        height:parent.parent.height/1.8
                        anchors.verticalCenter: parent.verticalCenter;
                        x: parent.parent.checked ?
                        parent.width-(width+(parent.height-height)/2):
                        (parent.height - height)/2
                        color: parent.parent.down?
                        "#2E2F57":
                        (parent.parent.checked?"#FFFFFF": "#960f133d")
                        radius: width/2
                    }
                }
                contentItem: Text {
                    text: parent.text
                    font: parent.font
                    leftPadding: parent.indicator.width+parent.spacing
                    color: parent.down? "#BDB5B5":
                           (parent.checked==true?"#FFFFFF": "#FFFFFF")
                    verticalAlignment: Text.AlignVCenter
                    opacity: (enabled == true)? 1.0: 0.3
                }
            }

            Row {
                height:facade.toPx(88)
                visible:index < 7?1:0;
                anchors {
                    left: parent.left;
                    right:parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
                Image {
                    source: image
                    width: facade.toPx(sourceSize.width * 1.5)
                    height:facade.toPx(sourceSize.height* 1.5)
                }
                ComboBox {
                    id: comboBoxCitys;
                    model: citysModel;

                    height:facade.toPx(88)
                    visible:index== 2?1:0;

                    anchors {
                        left: parent.left;
                        right:parent.right
                        leftMargin: facade.toPx(30)
                    }

                    font.pixelSize: facade.doPx(24)
                    background:Rectangle{opacity:0}

                    Component.onCompleted: citysModel.clear()

                    onActivated: {
                    textFieldInRow.text=textAt(currentIndex);
                    popup.close()
                    }

                    contentItem: Text {
                        opacity: 0
                        font: comboBoxCitys.font
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: comboBoxCitys.indicator.width + comboBoxCitys.spacing;
                    }

                    indicator: Canvas {
                        contextType: "2d"
                        width: facade.toPx(40)
                        height:facade.toPx(24)
                        x:comboBoxCitys.width - width - comboBoxCitys.rightPadding
                        y:comboBoxCitys.topPadding+(comboBoxCitys.availableHeight-height)/2

                        Connections {target:comboBoxCitys}

                        onPaint: {
                            context.reset();
                            context.moveTo(0,0);
                            context.lineTo(width, 0);
                            context.fillStyle = "#FFFFFF";
                            context.lineTo(width/2,height)
                            context.closePath();
                            context.fill();
                        }
                    }
                }
                TextField {
                    id: textFieldInRow
                    height:facade.toPx(88)
                    anchors {
                        left: parent.left;
                        right:parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(30)
                    }
                    placeholderText:plaseholder

                    onTextChanged: loader.fields[index]=text

                    color: "#FFFFFF"
                    inputMethodHints: (index == 6)?
                    Qt.ImhFormattedNumbersOnly:Qt.ImhNone

                    onFocusChanged: {
                        if(text.length== 0 && index == 6)
                        text = "+7"
                    }

                    font.family:museoSansMidle.name
                    font.pixelSize: facade.doPx(28)
                    background:Rectangle{opacity:0}
                }
            }

            Rectangle {//ебаное нижне подчеркивание
                height: 1
                color: "#FFFFFF"
                visible: index < 7?1:0
                anchors {
                    left: parent.left;
                    right:parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
            }
        }
    }

    Button {
        anchors {
            top: parent.top;
            left:parent.left
            topMargin: facade.toPx(25);
            leftMargin:facade.toPx(20);
        }
        id: mainScreenBackButton
        background:Rectangle{opacity:0}

        height:hambrButtonImage.height;
        width: hambrButtonImage.width + backText.implicitWidth

        Image {
            id: hambrButtonImage
            width: facade.toPx(sourceSize.width * 1.5);
            height:facade.toPx(sourceSize.height* 1.5);
            source:"ui/buttons/backButton.png"
            fillMode: Image.PreserveAspectFit;
        }

        onClicked:
            loader.goTo("qrc:/login.qml")
    }
    Text {
        id: backText
        color: "#FFFFFF"
        font.pixelSize: facade.doPx(22);
        font.family: museoSansMidle.name
        horizontalAlignment: Text.AlignHCenter

        text: "Вход"

        y: mainScreenBackButton.y + mainScreenBackButton.height/2 - implicitHeight/2
        x: mainScreenBackButton.x + mainScreenBackButton.width - implicitWidth
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }
}
//-------------------------------------------------------------------------------------------------------------
