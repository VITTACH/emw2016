#include <QFile>
#include <QScreen>
#include <wrapper.h>
#include <QQmlContext>
#include <QQuickStyle>
#include <QApplication>
#include "eventhandler.h"
#include "colorimageprovider.h"
#include <QQmlApplicationEngine>

bool isAndroid = false;

#ifdef Q_OS_ANDROID
#include "quickandroid.h"
#include "qadrawableprovider.h"
#include "qasystemdispatcher.h"
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>

JNIEXPORT jint JNI_OnLoad(JavaVM * javm, void*) {
    Q_UNUSED(javm);
    isAndroid=true;
    // It will must call this function within JNI_OnLoad to enable System Dispatcher
    QASystemDispatcher::registerNatives();
    return JNI_VERSION_1_6;
}
#endif

int main(int argc, char *argv []) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    EventHandler eventhandler;
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:///");
    QQuickStyle::setStyle("EmwStyle");
    QFile file(eventhandler.PathFile);
    QQmlContext *context = engine.rootContext();
    Wrapper jwr;
    context->setContextProperty("caller", &jwr);
    context->setContextProperty("curDirPath",QGuiApplication::applicationDirPath());
    context->setContextProperty("event_handler", &eventhandler);

    eventhandler.isAndroid= isAndroid;
    if(file.open(QIODevice::ReadOnly)) {
        eventhandler.isPin = true;
        file.close();
    }

    engine.addImageProvider("imageprovider", new ColorImageProvider(QQmlImageProviderBase::Image));
    engine.load(QUrl("qrc:/loads.qml"));
    QObject *root_obj = engine.rootObjects().first();
    QObject *loader = root_obj->findChild <QObject*> ("loader");
    loader->setProperty
    ("dpi",QApplication::screens().at(0)->logicalDotsPerInch());

    return app.exec();
}
