#ifndef HTTPNETWORK_H
#define HTTPNETWORK_H

#include <QFile>
#include <QHttpPart>
#include <QEventLoop>
#include <QtNetwork/QNetworkReply>

using namespace std;

class httpnetwork
{
public:
    string sessID = "";
    QNetworkReply *reply;
    QNetworkRequest *request;
    QNetworkAccessManager manager;

    void waiting();
    void setUrl(QUrl*);
    void httpsettsessionID();

public slots:
    QString sendGet();
    QString sendAvatar(QFile *);
    QString sendPost(QByteArray*);

};

#endif // HTTPNETWORK_H
