#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include "user.h"
#include "httpnetwork.h"

using namespace std;

struct Dialogs {
    int index;
    bool readed;
    Dialogs(int index,bool readed) {
        this->index = index;
        this->readed=readed;
    }
};

class EventHandler: public QObject {
    Q_OBJECT

public:
    explicit EventHandler (QObject * parent = 0);

    User user;

    bool isPin = 0;
    bool wasInChat = 0;
    bool isAndroid = 0;
    bool ismanymembers=false;

    int currentCity = 0;
    int currentEvent = 0;
    int currentDialog = 0;
    int currentMember = 0;
    int currentSpeaker[5];
    int curMemberPage = 0;
    int currentMemberIndex = -2;

    string response ="";
    QString PathFile="emw_2016";

    vector < Dialogs > myDialogs;
    vector < User > eventsArray;
    vector < User > membersArray;
    vector < QString > citysArray;
    httpnetwork httpPost, httpGet;

    vector <User> speakersVectArray[5];

    QString domain = "http://forum.app-labs.ru/";

    ~EventHandler();
signals:

public slots:
    bool isPined();
    bool itWasInChat();
    bool itIsAndroid();
    bool isManyMembers();
    bool parseReadResponse();
    bool getMessageFlag(int);
    bool applications(QString, QString, QString, QString);
    bool updatedUsers(QString, QString, QString, QString, QString, QString, QString);

    int networkLogin(QString phoneField = "", QString passField = "");
    int registration(QString, QString, QString, QString, QString, QString, QString, bool);

    bool postSpeaker(QString, QString);
    void fromHistory(int, QString, QString, QString, QString, QString, QString, QString, QString, int);

    void savingToFile(QString,QString);
    void sendAvatar(QString);
    void exitMenu();
    void getCitys();
    void setFamily(QString);
    void setPicture(QString);
    void setPhone(QString);
    void setLogin(QString);
    void copyText(QString);
    void setDescrip(QString);
    void setCompany(QString);
    void setMessage(QString, int, bool flag=false);
    void setPass(QString);
    void setMail(QString);
    void setPost(QString);
    void setCity(QString);
    void readMsgChat(int);
    void readMessage(int);
    void setInChat(bool);
    void getSpeakers(int, int);
    void getMembers(int,int);
    void curMemberIndex(int);
    void setGender(int);
    void getEvents();

    int getDialog(int index);
    QString getMemberDescrip(int ver);
    QString getMemberFamily(int ver);
    QString getMemberPhone(int ver);
    QString getMemberLogin(int ver);
    int getMemberId(int ve);
    int findMemberById(int);
    int getMemberIndex();
    QString getMemberPicture(int ver);
    QString getMemberCompany(int ver);
    int getMemberById(int);
    QString getMemberPost(int ver);
    QString getMemberMail(int ver);

    QString takeSpeaker(int, int);
    QString getMessage(int, int);
    QString getPicture();
    QString getDescrip();
    QString takeMember(int i = 0);
    QString getFamily();
    QString getPass();
    QString getCity();
    QString getPost();
    QString getMail();
    QString takeDialog(int i = 0);
    QString takeEvent (int i = 0);
    QString getCompany();
    QString getPhone();
    QString getLogin();
    QString takeCity();

    int getGender();
    int getId();
};

#endif // EVENTHANDLER_H
