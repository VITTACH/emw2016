#ifndef USER_H
#define USER_H
#include <vector>
#include <iostream>

using namespace std;

struct msg{
    bool flag;
    string message;
};

class User
{
private:
    int id=-1;
    int status;
    int gender;

    string time;
    string city;
    string post;
    string mail;
    string theme;
    string number;
    string picture;
    string company;
    string ssid= "";
    string userName;
    string lastName;
    string password;
    string response;
    string descript="";

    int currentMessage;
    vector<msg>message;

public:
    User();

    int getId();

    int getStatus();

    void setId(int);

    int getGender();

    string getPost();

    string getCity();

    string getSsid();

    string getMail();

    string getTime();

    string getTheme();

    string getNumber();

    string getDescrip();

    string getPicture();

    void setStatus(int);

    void setGender(int);

    void setCity(string);

    void setPost(string);

    void setMail(string);

    void setSsid(string);

    void setTime(string);

    void setNumber(string);

    string getCompany();

    string getResponse();

    string getUserName();

    string getLastName();

    string getPassword();

    string getMessage(int);

    string getEndMessage();

    bool getMessageFlag();

    void setTheme(string);

    void setDescrip(string);

    void setMessage(string, bool flag=false);

    void setCompany(string);

    void setPicture(string);

    void setPassword(string);

    void setUserName(string);

    void setLastName(string);

    void setResponse(string);

    bool getEndMessageFlag();
};

#endif // USER_H
