import QtQuick 2.0
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootScreen

    Timer {
        id: uploader
        repeat: true
        running: true
        interval: 100
        onTriggered:{
            if (threScreenDateList.ready) {
                upload.start()
                threScreenDateList.ready = false
            }
        }
    }

    Timer {
        id: upload
        interval: 500
        onTriggered:{
            event_handler.getSpeakers(3,spickersModel.count)
            var name;
            var time;
            var post;
            var comp;
            var them;
            var avatar;
            while(((avatar = event_handler.takeSpeaker(2,0)) != "FATAL_ERROR")
                  &&((name = event_handler.takeSpeaker(2,1)) != "FATAL_ERROR")
                  &&((post = event_handler.takeSpeaker(2,2)) != "FATAL_ERROR")
                  &&((comp = event_handler.takeSpeaker(2,3)) != "FATAL_ERROR")
                  &&((time = event_handler.takeSpeaker(2,4)) != "FATAL_ERROR")
                  &&((them = event_handler.takeSpeaker(2,5)) != "FATAL_ERROR")
                  ) {
                    spickersModel.append({
                        image0: avatar,
                            fullName: name,
                                post: post,
                                    company: comp,
                                        time: time,
                                            theme: them
                    });
                    event_handler.takeSpeaker(2,-1)
            }
            busyIndicator.visible = false
        }
    }

    ListView {
        Component.onCompleted: {
            spickersModel.clear()
            busyIndicator.visible = true;
        }

        id: listView
        anchors.fill: parent
        spacing: facade.toPx(10);
        boundsBehavior: Flickable.StopAtBounds

        model:  ListModel {
                id: spickersModel
                ListElement {
                    image0: "";
                    fullName:"";
                    post: "";
                    company: "";
                    time: "";
                    theme:"";
                    /*
                    icolor1: "#B9C22B"
                    icolor2: "#00FFFFFF"
                    icolor3: "#00FFFFFF"
                    icolor4: "#00FFFFFF"
                    icolor5: "#00FFFFFF"
                    */
                }
            }
        delegate: Rectangle {
            id: myrow
            clip: true
            color: "#7680B1"
            width: parent.width;
            height: Math.max(bug.height,
                             textname.implicitHeight +
                             textPost.implicitHeight +
                             textTime.implicitHeight +
                             textTheme.implicitHeight
                             ) + facade.toPx(40)
            MouseArea {
                id: navMouseArea
                anchors.fill: parent

                onPressed: {myrow.color = "#51587F";}
                onReleased:{myrow.color = "#7680B1";}
                onPositionChanged:myrow.color="#7680B1"

                onClicked: {
                    if(textname.text != "Перерыв " && textname.text != "") {
                    loader.chatVariable = 3;
                    loader.currentChatIndex = index;
                    event_handler.curMemberIndex(index)
                    loader.goTo("qrc:/memberProfile.qml")
                    }
                }
            }

            /*
            Image {
                x: parent.width/2 - width/2
                y: (parent.width < parent.height)?
                    parent.height/2 - height/2: 0;
                height:(parent.width < parent.height)?
                    parent.height:
                    sourceSize.height*(width/sourceSize.width)
                width: (parent.width < parent.height)?
                    sourceSize.width*
                    (parent.height/sourceSize.height):
                        parent.width;
                source: image
            }
            Row {
                anchors.fill: parent
                Rectangle {
                    color: icolor1
                    height: parent.height
                    width: icolor1=="#00FFFFFF"? 0:facade.toPx(15)
                }
                Rectangle {
                    color: icolor2
                    height: parent.height
                    width: icolor2=="#00FFFFFF"? 0:facade.toPx(15)
                }
                Rectangle {
                    color: icolor3
                    height: parent.height
                    width: icolor3=="#00FFFFFF"? 0:facade.toPx(15)
                }
                Rectangle {
                    color: icolor4
                    height: parent.height
                    width: icolor4=="#00FFFFFF"? 0:facade.toPx(15)
                }
                Rectangle {
                    color: icolor5
                    height: parent.height
                    width: icolor5=="#00FFFFFF"? 0:facade.toPx(15)
                }
            }
            */
            Item {
                anchors.fill:parent
                anchors.leftMargin:facade.toPx(20)
                Rectangle {
                    id: bug
                    clip: true
                    smooth: true
                    visible: false
                    width: facade.toPx(270)
                    height:facade.toPx(270)
                    anchors {
                        top: parent.top
                        topMargin: facade.toPx(20)
                    }
                    /*
                    width: (textname.implicitHeight +
                            textPost.implicitHeight +
                            textTime.implicitHeight +
                            textTheme.implicitHeight>
                            facade.toPx(210))?
                               facade.toPx(270): facade.toPx(210);
                    height:(textname.implicitHeight +
                            textPost.implicitHeight +
                            textTime.implicitHeight +
                            textTheme.implicitHeight>
                            facade.toPx(210))?
                               facade.toPx(270): facade.toPx(210);
                               */

                    Image {
                        source: image0
                        height:sourceSize.width>sourceSize.height?
                               parent.height:
                               sourceSize.height * (parent.width/sourceSize.width)
                        width: sourceSize.width>sourceSize.height?
                               sourceSize.width*(parent.height/sourceSize.height):
                               parent.width
                    }
                }

                Image {
                    id: mask
                    smooth: true;
                    visible:false
                    source: "ui/uimask/roundMask.png"
                    sourceSize: Qt.size(bug.width,bug.height)
                }

                OpacityMask {
                    source: bug
                    maskSource: mask
                    anchors.fill:bug
                }
                Column {
                    width: parent.width/ 2;
                    anchors {
                        right: parent.right
                        rightMargin: facade.toPx(20)
                        verticalCenter: parent.verticalCenter
                    }
                    Text {
                        id: textname
                        color:"#FFFFFF"
                        text: fullName;
                        styleColor: "black"
                        style: Text.Raised;
                        wrapMode: Text.Wrap
                        width: parent.width - facade.toPx(20)
                        font {
                            bold: true;
                            pixelSize: facade.doPx(30);
                            family: museoSansMidle.name
                        }
                    }
                    Text {
                        id: textPost
                        color: "#FFFFFF"
                        styleColor: "black"
                        style: Text.Raised;
                        wrapMode: Text.Wrap
                        width: parent.width - facade.toPx(20)
                        font {
                            pixelSize: facade.doPx(22);
                            family: museoSansMidle.name
                        }
                        text: company != ""?
                              (post + rootScreen.width > 550?
                                " в ":"\nв " + company): post
                    }
                    Text {
                        id: textTime
                        color: "#FFFFFF"
                        styleColor: "black"
                        style: Text.Raised;
                        wrapMode: Text.Wrap
                        width: parent.width - facade.toPx(20)
                        font {
                            pixelSize: facade.doPx(18);
                            family: museoSansMidle.name
                        }
                        text: "\nВремя " + (textname.text != "Перерыв "? "выступления: ": "перерыва: ") + time;
                    }
                    Text {
                        id: textTheme
                        color: "#FFFFFF"
                        styleColor: "black"
                        style: Text.Raised;
                        wrapMode: Text.Wrap
                        width: parent.width - facade.toPx(20)
                        font {
                            pixelSize: facade.doPx(18);
                            family: museoSansMidle.name
                        }
                        text: "\nТема: " + theme
                    }
                }
            }
        }
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }
}
