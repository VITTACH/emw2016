import QtQuick 2.7
import QtQuick.Controls 2.0
import"EmwStyle" as EmwStyle

Item {
    id: rootScreen

    EmwStyle.NavigateDrawer
    {id:profileScreenDrawer}

    EmwStyle.ChatinListener
    {id:chatingecholistener}

    ListView {
        id: listView
        width: parent.width;
        anchors {
            bottom:parent.bottom
            topMargin: facade.toPx(50)
            top: profileScreenNavbarMenu.bottom
        }
        boundsBehavior: Flickable.StopAtBounds;

        Component.onCompleted:{
            profileModel.clear()
            profileModel.append({
                image: "ui/profiles/phoneIcon.png",
                target1: "Телефоны",
                target2: "+7(343)213-33-47\n+7(932)613-88-77"
            });
            profileModel.append({
                image: "ui/profiles/mailIcon.png",
                target1: "E-mail",
                target2: "zg-event@list.ru"
            });
            profileModel.append({
                image: "ui/profiles/webIcon.png",
                target1: "Web-site",
                target2: "www.e-m-w.ru"
            });
            if (event_handler.itIsAndroid() == true)
                profileModel.append({
                    image: "ui/screensIcons/vk.png",
                    target1: "VK",
                    target2: "www.vk.com/emw2016"
                });
            profileModel.append({
                image: "ui/screensIcons/fb.png",
                target1: "FB",
                target2: "EurasianMarketingWeek"
            });
        }

        model:  ListModel {
            id: profileModel
                /*
                ListElement {
                    image: "";
                    target1: "";
                    target2: "МОСКВА 'Арт Отель' ул. 3-я Песчаная, д. 2\n(495) 725-09-05";
                }
                ListElement {
                    image: "ui/maps/map1.png";
                    target1: "";
                    target2: "";
                }
                ListElement {
                    image: "";
                    target1: "";
                    target2: "ЕКАТЕРИНБУРГ Бахчиванжди, 55а\n(343) 272-65-55";
                }
                ListElement {
                    image: "ui/maps/map2.png";
                    target1: "";
                    target2: "";
                }
                */
            }
        delegate: Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: text1.implicitHeight + text2.implicitHeight + facade.toPx(40);
            /*
            height: (index < 4 || index == 5)?
                    text1.implicitHeight + text2.implicitHeight:
                    imageIcon.height+facade.toPx(40)
                    */
            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                Rectangle {
                    id: myrow
                    x: listView.width /2 - width /2;
                    width: facade.toPx(500)
                    height: text1.implicitHeight + text2.implicitHeight;
                    /*
                    width: (index < 3)?facade.toPx(500):facade.toPx(750)
                    height: (index < 4||index == 5)?
                            text1.implicitHeight + text2.implicitHeight:
                            imageIcon.height
                            */

                    MouseArea {
                        anchors.fill: parent
                        propagateComposedEvents:true
                        onClicked: {
                            switch(index) {
                            case 2:
                                Qt.openUrlExternally("http://" + target2)
                                break;
                            case 3:
                                Qt.openUrlExternally("http://" + target2)
                                break;
                            case 4:
                                Qt.openUrlExternally("http://www.fb.com/" + target2)
                                break;
                            }
                        }
                    }

                    Item {
                        anchors.fill: parent
                        Image {
                            width: facade.toPx(sourceSize.width);
                            height:facade.toPx(sourceSize.height)
                            id: imageIcon;
                            source: image;
                        }
                        Column {
                            spacing: facade.toPx(5);
                            anchors {
                                right: parent.right;
                                left:imageIcon.right
                                leftMargin: (index == 0)? facade.toPx(10): facade.toPx(20)
                            }
                            Text {
                                id: text1
                                text: target1
                                color: "#000000"
                                width: parent.width;
                                wrapMode: Text.Wrap;
                                horizontalAlignment: Text.AlignHCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                font {
                                    bold: true
                                    family: museoSansMidle.name
                                    pixelSize:(index!=0)? facade.doPx(22): facade.doPx(24)
                                }
                            }
                            Text {
                                id: text2
                                text: target2
                                color: "#000000"
                                width: parent.width;
                                //wrapMode: Text.Wrap;
                                elide: Text.ElideRight
                                horizontalAlignment: Text.AlignHCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                font {
                                    family: museoSansLight.name
                                    pixelSize:(index==0)? facade.doPx(24): facade.doPx(26)
                                }
                            }
                        }
                    }
                }
                Rectangle {// подчеркивание
                    height: 2
                    color: "#B8BDD6"
                    width: parent.width
                    opacity: (index<4)? 1:0
                }
            }
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(500)
        id: profileScreenNavbarMenu

        Image {
            x: rootScreen.width/2 - width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootScreen.width < rootScreen.height/1.5)?
                    rootScreen.height/2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootScreen.width < rootScreen.height/1.5)?
                    sourceSize.width*(rootScreen.height/2/sourceSize.height):
                       rootScreen.width;
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton;
            width:2*hambrButtonImage.width;
            height:hambrButtonImage.height;
            background:Rectangle{opacity:0}

            onClicked: profileScreenDrawer.open();

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Column {
            spacing: facade.toPx(30)
            anchors.centerIn: parent
            anchors.leftMargin: profileDrawerButton.y + profileDrawerButton.width

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Евразийская Неделя\nМаркетинга 2016"
            }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                width: facade.toPx(sourceSize.width / 1.1);
                height:facade.toPx(sourceSize.height/ 1.1);
                source: "ui/loginlogo.png"
            }
        }
    }

    EmwStyle.DialogWindow{
        id: dialogAndroid;
    }
}
