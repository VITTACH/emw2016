import QtQuick 2.7
import QtQuick.Controls 2.0
import "EmwStyle" as EmwStyle

Item {
    id: rootMembers

    EmwStyle.NavigateDrawer
    {id : memberScreenDrawer}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Timer {
        repeat: true
        running: true
        interval: 100
        onTriggered: {
            if(dateModel.get(view.currentIndex).iheigh != 20) {
                dateModel.setProperty(view.currentIndex,"iheigh",20)
                switch(view.currentIndex) {
                    case 0: {oneScreenDateList.ready = true; break;}
                    case 1: {twoScreenDateList.ready = true; break;}
                    case 2: threScreenDateList.ready = true; break;
                    case 3: fourScreenDateList.ready = true; break;
                    case 4: fiveScreenDateList.ready = true; break;
                }

            loader.currentDateList= view.currentIndex
            for (var i = 0; i < dateModel.count; i++)
                if(i != view.currentIndex) {
                dateModel.setProperty(i,"iheigh", 10)

                switch(i) {
                    case 0: {oneScreenDateList.ready= false; break;}
                    case 1: {twoScreenDateList.ready= false; break;}
                    case 2: threScreenDateList.ready = false; break;
                    case 3: fourScreenDateList.ready = false; break;
                    case 4: fiveScreenDateList.ready = false; break;
                }
                }
            }
        }
    }

    SwipeView {
        id: view
        currentIndex: loader.currentDateList

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: memberScreenNavbarMenu.bottom
        }

        Loader {
            id: oneScreenDateList
            property bool ready: false
            Component.onCompleted: source = "qrc:/memberOrderList1.qml"
        }

        Loader {
            id: twoScreenDateList
            property bool ready: false
            Component.onCompleted: source = "qrc:/memberOrderList2.qml"
        }

        Loader {
            id: threScreenDateList
            property bool ready: false
            Component.onCompleted: source = "qrc:/memberOrderList3.qml"
        }

        Loader {
            id: fourScreenDateList
            property bool ready: false
            Component.onCompleted: source = "qrc:/memberOrderList4.qml"
        }

        Loader {
            id: fiveScreenDateList
            property bool ready: false
            Component.onCompleted: source = "qrc:/memberOrderList5.qml"
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(200)
        id: memberScreenNavbarMenu
        Image {
            y: -(height-facade.toPx(360));
            x: rootMembers.width/2-width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootMembers.width< rootMembers.height/2)?
                   rootMembers.height/2:
                   sourceSize.height*(width/sourceSize.width)
            width: (rootMembers.width< rootMembers.height/2)?
                   sourceSize.width*(rootMembers.height/2/sourceSize.height):
                       rootMembers.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton
            width:2*hambrButtonImage.width
            height:hambrButtonImage.height

            background: Rectangle { opacity: 0; }

            onClicked: loader.back()//memberScreenDrawer.open();

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 1.5)
                height:facade.toPx(sourceSize.height* 1.5 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/backButton.png"
            }
        }

        Text {
            color: "#FFFFFF"
            font {
                bold: true;
                pixelSize: facade.doPx(24);
                family: museoSansMidle.name
            }

            horizontalAlignment: Text.AlignHCenter
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: facade.toPx(34)
                leftMargin: parent.width/2 - facade.toPx(50)
                rightMargin:parent.width/2 - facade.toPx(50)
            }

            text: "EMW 2016"
        }

        Row {
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Repeater {
                model: ListModel {
                    id: dateModel
                    ListElement {
                        iheigh: 10
                        city: "Москва"
                        target: "1 декабря"
                        icolor: "#B9C22B"
                    }
                    ListElement {
                        iheigh: 10
                        city: "Екатеринбург"
                        target: "2 декабря"
                        icolor: "#FEC407"
                    }
                    ListElement {
                        iheigh: 10
                        city: "Екатеринбург"
                        target: "3 декабря"
                        icolor: "#B71679"
                    }
                    ListElement {
                        iheigh: 10
                        city: "Екатеринбург"
                        target: "4 декабря"
                        icolor: "#73CA15"
                    }
                    ListElement {
                        iheigh: 10
                        city: "Екатеринбург"
                        target: "4 декабря"
                        icolor: "#913694"
                    }
                }
                delegate: Rectangle {
                    color: "#00FFFFFF"
                    height: facade.toPx(80)
                    width: rootMembers.width/dateModel.count
                    MouseArea {
                        anchors.fill:parent
                        propagateComposedEvents: true

                        onClicked: {
                            if (iheigh == 10) {
                                iheigh = 20;
                                switch(index) {
                                case 0: oneScreenDateList.ready = true; break
                                case 1: twoScreenDateList.ready = true; break
                                case 2: threScreenDateList.ready= true; break
                                case 3: fourScreenDateList.ready= true; break
                                case 4: fiveScreenDateList.ready= true; break
                                }
                                for(var i = 0; i < dateModel.count; i++)
                                    if(i != index) {
                                        dateModel.setProperty(i,"iheigh",10);
                                        switch(i) {
                                        case 0:oneScreenDateList.ready= false
                                            break;
                                        case 1:twoScreenDateList.ready= false
                                            break;
                                        case 2:threScreenDateList.ready=false
                                            break;
                                        case 3:fourScreenDateList.ready=false
                                            break;
                                        case 4:fiveScreenDateList.ready=false
                                            break;
                                        }
                                    }
                            }
                            currentDateList=index;
                        }
                    }
                    Item {
                        anchors.fill:parent
                        Text {
                            id: dateText
                            color: "#FFFFFF"
                            font.pixelSize: facade.doPx(16);
                            font.family: museoSansMidle.name
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: target
                        }
                        Text {
                            color: "#FFFFFF"
                            font.pixelSize: facade.doPx(10);
                            font.family: museoSansMidle.name
                            anchors {
                                top: dateText.bottom
                                horizontalCenter: parent.horizontalCenter
                            }
                            text: "(" + city + ")"
                        }
                        Rectangle {
                            color: icolor
                            width: parent.parent.width
                            height:facade.toPx(iheigh)
                            anchors.bottom: parent.bottom
                        }
                    }
                }
            }
        }
    }

    EmwStyle.DialogWindow{
        id: dialogAndroid;
    }
}
