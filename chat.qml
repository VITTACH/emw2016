import QtQuick 2.7
import QtWebSockets 1.0
import QtQuick.Controls 2.0
import"EmwStyle"as EmwStyle

Item {
    id: rootChat

    EmwStyle.NavigateDrawer
    {id: chatScreenDrawer;}

    function parseToJSON(message){
        var obj = {
            type: 'message',
            data: {
                user: {
                    id: event_handler.getMemberId(loader.chatVariable)
                },
                me: {
                first_name: event_handler.getLogin(),
                last_name: event_handler.getFamily(),
                },
                msg: {
                msg_text : message
                }
            }
        }
        return JSON.stringify(obj)
    }

    Timer {
        repeat: loader.chatVariable<1?1:0
        running:loader.chatVariable<1?1:0
        interval: 10;
        Component.onCompleted: {
            event_handler.setInChat(true)
            event_handler.getMessage(event_handler.getMemberIndex(), -1)
        }
        onTriggered:{
            event_handler.readMsgChat(event_handler.getMemberIndex())
        if (loader.reloadChat == true) {
            loader.reloadChat = false;
            event_handler.getMessage(event_handler.getMemberIndex(), -1)
            navBarLogoName.text = event_handler.getMemberLogin(0)+ " " +event_handler.getMemberFamily(0)
            chatModel.clear();
        }

        if(event_handler.getMemberIndex()!=loader.currentChatIndex){
            dialogAndroid.text="Вам поступило сообщение. Прочитать?"
            dialogAndroid.visible = true
        }
        else
        if (event_handler.getMessage(event_handler.getMemberIndex(), 0).length != 0) {
            if(!busyIndicator.visible) busyIndicator.visible = true;
            buferText.text=event_handler.getMessage(event_handler.getMemberIndex(), 1)
            var flag = event_handler.getMessageFlag(event_handler.getMemberIndex())
            appendMessage(buferText.text,flag)
            chatScreenChatList.positionViewAtEnd()
        }
        else if(busyIndicator.visible) busyIndicator.visible = false
        }
    }

    function appendMessage(message, flag) {
        chatModel.append(
        {
            someText: message,
            myheight: buferText.contentHeight,
            mySpacing: (flag == 0)? 2 : facade.toPx(20),
            textColor: (flag == 0)? "#000000":"#FFFFFF",
            backgroundColor: (flag == 0)? "#E5E5EA":"#51587F",
            HorizonPosition: flag*(parent.width/2 - facade.toPx(60)) + facade.toPx(30),
            image: flag == 0? "ui/screensIcons/chatMessageWhite.png"
                              :"ui/screensIcons/chatMessageBlue.png"
        });
    }

    Timer {
        id: navBarTimer
        interval: 2000;
        //onTriggered: chatScreenNavbarMenu.y = rootChat.height*0.45
    }

    ListView {
        id: chatScreenChatList
        model: ListModel {id: chatModel}

        anchors {
            left: parent.left
            right: parent.right
            topMargin: facade.toPx(50)
            bottom: flickableTextArea.top
            top: chatScreenNavbarMenu.bottom
        }

        delegate: Item {
            height: (facade.toPx(65) - myheight>=0)?
                     mySpacing + facade.toPx(65):
                     mySpacing + facade.doPx(26) + myheight
            Image {
                source: image
                width: facade.toPx(sourceSize.width * 1.2)
                height:facade.toPx(sourceSize.width * 1.2)
                y: parentText.y +parentText.height-sourceSize.height
                x: (parentText.x== facade.toPx(30))?
                    parentText.x - width/2:
                    parentText.x + parentText.width - width/2
            }
            TextArea {
                id: parentText
                text: someText
                x: HorizonPosition
                width: rootChat.width/2
                height: (facade.toPx(65)-myheight >= 0)?
                         facade.toPx(65): myheight + facade.doPx(26)

                background: Rectangle {
                    color: backgroundColor;
                    radius: facade.toPx(25)
                }

                readOnly: true;
                color:textColor
                wrapMode: TextEdit.Wrap
                verticalAlignment: Text.AlignVCenter

                font.family: museoSansLight.name
                font.pixelSize: facade.doPx(26);
            }
        }
    }

    Rectangle {
        radius: facade.toPx(25)
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: flickableTextArea.top
            leftMargin: 0.02*parent.width
            rightMargin:0.02*parent.width
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(140)
        id: chatScreenNavbarMenu
        Image {
            y: -(height-facade.toPx(280))
            x: rootChat.width/2 - width/2

            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootChat.width < rootChat.height/2)?
                    rootChat.height / 2: (width / sourceSize.width) * sourceSize.height
            width: (rootChat.width < rootChat.height/2)?
                    sourceSize.width*rootChat.height/2/sourceSize.height:rootChat.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            onClicked: {
                loader.back();
                /*
                chatScreenDrawer.open()
                chatScreenNavbarMenu.y = 0
                */
            }

            id: profileDrawerButton
            width:2*hambrButtonImage.width
            height:hambrButtonImage.height

            background: Rectangle { opacity: 0; }

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 1.5)
                height:facade.toPx(sourceSize.height* 1.5 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/backButton.png"
            }
        }

        Column {
            anchors {
                centerIn: parent
                leftMargin: profileDrawerButton.y + profileDrawerButton.width
            }
            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("EMW 2016 Чат")
            }

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: event_handler.getMemberLogin(loader.chatVariable) + " " + event_handler.getMemberFamily(loader.chatVariable)
            }
        }
    }

    TextArea {
        visible:false
        id: buferText
        wrapMode: TextEdit.Wrap
        width: rootChat.width/2
        font.pixelSize: facade.doPx(26);
        font.family: museoSansLight.name
    }

    Flickable {
        id: flickableTextArea
        flickableDirection: Flickable.VerticalFlick
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            bottomMargin: facade.toPx(25)
            leftMargin: 0.02*parent.width
            rightMargin:0.03*parent.width + chatScreenPostButton.width
        }

        height: (screenTextFieldPost.lineCount < 5)?
                 facade.toPx(70) +
                 (screenTextFieldPost.lineCount -1) * facade.doPx(43):
                 facade.toPx(70) +4*facade.doPx(43)

        TextArea.flickable:
            TextArea {
            id: screenTextFieldPost

            background: Rectangle {
                border.width: 2
                border.color: "#C8C8C8"
                radius: facade.toPx(25)
            }

            color: "#000000"
            wrapMode: TextEdit.Wrap
            font.pixelSize: facade.doPx(26);

            font.family: museoSansLight.name
            placeholderText: ("Написать...")
            verticalAlignment: Text.AlignVCenter

            onActiveFocusChanged: {
                if (activeFocus)
                navBarTimer.start()
                else
                    chatScreenNavbarMenu.y=0
            }
        }
    }

    Button {
        y: flickableTextArea.y
        id: chatScreenPostButton

        height:buttonImage.height
        width: buttonImage.width + facade.toPx(10)

        anchors {
            right: parent.right
            rightMargin: 0.02 * parent.width;
        }
        background: Rectangle { opacity : 0 }

        Image {
            id: buttonImage
            x: facade.toPx(10)
            fillMode: Image.PreserveAspectFit;
            source:"ui/buttons/sendButton.png"
            height: facade.toPx(sourceSize.height*1.5)
        }

        onClicked: {
            if (screenTextFieldPost.text.length >= 1) {
                buferText.text=screenTextFieldPost.text
                screenTextFieldPost.text=""
                appendMessage(buferText.text, 0);
                chatScreenChatList.positionViewAtEnd();
                chatScreenNavbarMenu.y = 0;

                if(loader.chatVariable < 1) {
                event_handler.setMessage(buferText.text, event_handler.getMemberIndex(), true)
                socket.sendTextMessage(parseToJSON(buferText.text))
                }
                else
                    event_handler.postSpeaker(buferText.text, event_handler.getMemberLogin(1))
            }
        }

        contentItem: Text {
            color: chatScreenPostButton.down? "#0f133d" : "#7680B1"
            horizontalAlignment:Text.AlignHCenter
            verticalAlignment : Text.AlignVCenter
            opacity: enabled?1:0.3
            elide: Text.ElideRight
        }
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }
}
