import QtQuick 2.7

//--------------------------------------------------basicScreen.qml--------------------------------------------
Item {
    Timer {
        running: true
        interval:2000
        onTriggered: event_handler.isPined()?
                     (
                         event_handler.networkLogin()==1?
                         loader.goTo("qrc:/profile.qml"):
                            loader.goTo("qrc:/login.qml")
                     ):
                     loader.goTo("qrc:/login.qml")
    }

    Image {
            x:  parent.width / 2 - width / 2;
            y: (parent.width < parent.height)?
                parent.height/ 2 - height/ 2:0
        height:(parent.width < parent.height)?
                parent.height:
                (width/sourceSize.width)*sourceSize.height
        width: (parent.width < parent.height)?
                sourceSize.width*parent.height
                /sourceSize.height:
                parent.width
        source: "ui/background/background1.png"
    }

    Image {
        id: imagelogo
        y: 0.5*parent.height-height/2
        x: parent.width/2 - imagelogo.width/2
        width: sourceSize.width/ 1.5 * parent.width/500
        height:sourceSize.height/1.5 * parent.width/500
        source: "ui/mainlogo.png"
    }
}
//-------------------------------------------------------------------------------------------------------------
