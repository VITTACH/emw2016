import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootProfile

    EmwStyle.NavigateDrawer
    {id: profileScreenDrawer}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    function openingChat() {
        if(loader.chatVariable < 1){
        var ind =event_handler.getMemberId(loader.chatVariable)
        var pos =event_handler.findMemberById(ind)
        if (pos < 0)
            pos = event_handler.getMemberById(ind)
        event_handler.curMemberIndex(pos);
        loader.currentChatIndex=pos;
        }
        loader.goTo("qrc:/chat.qml")
    }

    Rectangle {
        z: 1
        clip: true
        width: parent.width
        height: facade.toPx(350)
        id: profileScreenNavbarMenu

        Image {
            x: rootProfile.width/2-width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootProfile.width<rootProfile.height/1.5)?
                    rootProfile.height/ 2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootProfile.width<rootProfile.height/1.5)?
                    sourceSize.width*(rootProfile.height/2/sourceSize.height):
                        rootProfile.width;
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton
            width:2*hambrButtonImage.width
            height:hambrButtonImage.height

            background: Rectangle { opacity: 0; }

            onClicked: loader.back()//profileScreenDrawer.open();

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 1.5)
                height:facade.toPx(sourceSize.height* 1.5 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/backButton.png"
            }
        }

        /*
        Button {
            y: facade.toPx(41)
            id: profileEditButton
            anchors.right: parent.right
            width : editButtonImage.width
            height:editButtonImage.height
            anchors.rightMargin: facade.toPx(20)

            background: Rectangle { opacity: 0;}

            onClicked:openingChat()

            Image {
                id: editButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source:"ui/buttons/mailButton.png"
            }
        }
        */

        Text {
            id: logoText
            color: "#FFFFFF"
            font.pixelSize: facade.doPx(18);
            font.family: museoSansMidle.name
            horizontalAlignment: Text.AlignHCenter

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: facade.toPx(34)
                leftMargin: parent.width /2 - facade.toPx(50)
                rightMargin: parent.width/2 - facade.toPx(50)
            }

            text: "Евразийская Неделя\nМарктеинга 2016"
        }

        Text {
            id: fullNameText
            color: "#FFFFFF"
            font.pixelSize: facade.doPx(22);
            font.family: museoSansMidle.name
            horizontalAlignment: Text.AlignHCenter

            anchors {
                left: parent.left
                right: parent.right
                top: logoText.bottom
                topMargin: facade.toPx(30)
                leftMargin: parent.width /2 - facade.toPx(50)
                rightMargin: parent.width/2 - facade.toPx(50)
            }

            //text: "Дмитрий Васильев"
            text: event_handler.getMemberLogin(loader.chatVariable) + " " + event_handler.getMemberFamily(loader.chatVariable);
        }
    }

    ListView {
        id: listView
        width: parent.width
        anchors {
            bottom: parent.bottom
            topMargin: facade.toPx(200)
            top:profileScreenNavbarMenu.bottom
        }
        boundsBehavior: Flickable.StopAtBounds

        Component.onCompleted: {
            profileModel.clear()
            profileModel.append({
                image: "",
                target1:event_handler.getMemberPost(loader.chatVariable),
                target2:event_handler.getMemberCompany(loader.chatVariable)
            });
            if(loader.chatVariable < 1) {
            profileModel.append({
                image:"ui/profiles/phoneIcon.png",
                target1: "Номер телефона",
                target2:event_handler.getMemberPhone(loader.chatVariable)
            });
            profileModel.append({
                image: "ui/profiles/mailIcon.png",
                target1: "E-mail",
                target2: event_handler.getMemberMail(loader.chatVariable)
            });
            }
            if ((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
              ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49)
              ||event_handler.getMemberDescrip(loader.chatVariable)!="") {
                profileModel.append({
                    image: "",
                    target1: "",
                    target2: ((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
                            ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49))?
                                 "Коллеги, приветствую Вас!\n\nЯ, Котляров Данила, генеральный директор компании 'Лаборатория Мобильных Решений' или AppLabs LLC. Я и моя команда занимаемся тем, что создаем клевые мобильные приложения для бизнеса под ос Android, iOs, Windows Phone любой сложности.\nСоздаваемые нами приложения помогают решать такие задачи как:\n- Отстранение от конкурентов\n- Увеличение повторных продаж и среднего чека\n- Сокращение расходов на маркетинг и традиционную рекламу.\n- Получение от клиентов оперативной обратной связи\n- Повышение лояльности и узнаваемости бренда\nИ многие другие задачи.\nЗа подробностями обращайтесь через кнопку 'О разработчике', через кнопку 'Задать вопрос' или через соц. сети.\n\nС уважением, Котляров Данила.":
                                 event_handler.getMemberDescrip(loader.chatVariable)
                });
                if ((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
                  ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49)) {
                    if(event_handler.itIsAndroid())
                    profileModel.append({
                        image: "ui/screensIcons/vk.png",
                        target1: "VK",
                        target2: "vk"
                    });
                    profileModel.append({
                        image: "ui/screensIcons/fb.png",
                        target1: "FB",
                        target2: "fb"
                    });
                    profileModel.append({
                        image: "ui/screensIcons/in.png",
                        target1: "Instagram",
                        target2: "instagram"
                    });
                }
            }
            profileModel.append({
                image: "ui/profiles/mailButton.png",
                target1:loader.chatVariable<1?"Начать чат":"Задать вопрос",
                target2:event_handler.getMemberLogin(loader.chatVariable)+" "+event_handler.getMemberFamily(loader.chatVariable)
            });
        }

        model:  ListModel {
            id: profileModel
                ListElement {
                    image: "";
                    target1: "";
                    target2: "";
                }
            }
        delegate: Rectangle {
            anchors {
                left: parent.left;
                right:parent.right
            }
            height: text1.implicitHeight + text2.implicitHeight + facade.toPx(80)

            MouseArea {
                anchors.fill:parent
                onClicked: {
                    if(loader.chatVariable < 1) {
                        if(index == 1) {
                            if(!event_handler.itIsAndroid()) Qt.openUrlExternally("tel:" + target2)
                            else
                                caller.directCall("+"+target2)
                        }
                        if(index == 2) {
                            event_handler.copyText(target2)
                            dialogAndroid.text="Почта скопирована в буфер обмена"
                            dialogAndroid.visible = true;
                        }
                    }

                    if ((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
                      ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49)) {
                        if(loader.chatVariable < 1) {
                            if(event_handler.itIsAndroid()) {
                                if(index == 4)
                                    Qt.openUrlExternally("http://www.vk.com/danila_ejov")
                                if(index == 5)
                                    Qt.openUrlExternally("http://www.facebook.com/danilakotlyarov")
                                if(index == 6)
                                    Qt.openUrlExternally("http://www.instagram.com/busines_app")
                                if(index == 7) openingChat()
                            }
                            else {
                                if(index == 4)
                                    Qt.openUrlExternally("http://www.facebook.com/danilakotlyarov")
                                if(index == 5)
                                    Qt.openUrlExternally("http://www.instagram.com/busines_app")
                                if(index == 6) openingChat()
                            }
                        }
                        else {
                            if(event_handler.itIsAndroid()) {
                                if(index == 2)
                                    Qt.openUrlExternally("http://www.vk.com/danila_ejov")
                                if(index == 3)
                                    Qt.openUrlExternally("http://www.facebook.com/danilakotlyarov")
                                if(index == 4)
                                    Qt.openUrlExternally("http://www.instagram.com/busines_app")
                                if(index == 5) openingChat()
                            }
                            else {
                                if(index == 2)
                                    Qt.openUrlExternally("http://www.facebook.com/danilakotlyarov")
                                if(index == 3)
                                    Qt.openUrlExternally("http://www.instagram.com/busines_app")
                                if(index == 4) openingChat()
                            }
                        }
                    }
                    else {
                        if(event_handler.getMemberDescrip(loader.chatVariable)!="") {
                            if(loader.chatVariable < 1) {
                                if(index == 4) openingChat()
                            }
                            else {
                                if(index == 2) openingChat()
                            }
                        }
                        else {
                            if(loader.chatVariable < 1) {
                                if(index == 3) openingChat()
                            }
                            else {
                                if(index == 1) openingChat()
                            }
                        }
                    }
                }
            }

            Column {
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                Rectangle {
                    id: myrow
                    width: ((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
                            ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49))
                            &&((index==3&&loader.chatVariable < 1) || (index == 1 && loader.chatVariable >= 1))?
                               parent.width-facade.toPx(120):facade.toPx(600)
                    x: listView.width/2 - width/2;
                    height: text1.implicitHeight + text2.implicitHeight + facade.toPx(40)
                    Item {
                        anchors.fill:parent
                        Image {
                            width: facade.toPx(sourceSize.width);
                            height:facade.toPx(sourceSize.height)
                            id: imageIcon;
                            source: image;
                        }
                        Column {
                            spacing: facade.toPx(5);
                            anchors {
                                right: parent.right;
                                left:imageIcon.right
                                leftMargin: (index == 0)?facade.toPx(10): facade.toPx(20)
                            }
                            Text {
                                id: text1
                                text: target1
                                color:"#7680B1"
                                width: parent.width;
                                wrapMode: Text.Wrap;
                                font {
                                    bold: true
                                    family: museoSansMidle.name
                                    pixelSize: (index>0)?facade.doPx(24): facade.doPx(26)
                                }
                                horizontalAlignment: Text.AlignHCenter
                                anchors.horizontalCenter:parent.horizontalCenter
                            }
                            Text {
                                id: text2
                                text: target2
                                color: "#7680B1"
                                width: parent.width;
                                wrapMode: Text.Wrap;
                                font {
                                    family: museoSansMidle.name
                                    pixelSize: (index>0)?facade.doPx(26): facade.doPx(24)
                                }
                                horizontalAlignment: (((loader.chatVariable < 1 && event_handler.getMemberId(loader.chatVariable) == 15)
                                                       ||(loader.chatVariable > 0 && event_handler.getMemberId(loader.chatVariable) == 49))
                                                       && index==((loader.chatVariable<1)?3:1))? Text.AlignLeft: Text.AlignHCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                            }
                        }
                    }
                }
                Rectangle {// подчеркивание
                    height: 2
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    y: index * myrow.height
                    border.color: "#B8BDD6"
                }
            }
        }
    }

    Button {
        z: 2
        width: facade.toPx(300)
        height:facade.toPx(300)
        onClicked:openingChat()

        background:Rectangle{opacity:0}

        anchors.topMargin:- bug.width/2
        x: parent.width/2 - bug.width/2
        anchors.top:profileScreenNavbarMenu.bottom

        Rectangle {
            id: bug
            clip: true
            smooth: true
            visible: false
            width: parent.width;
            height:parent.height
            anchors.verticalCenter: parent.verticalCenter;
            Image {
                source:event_handler.getMemberPicture(loader.chatVariable)
                height:sourceSize.width>sourceSize.height?
                       parent.height:
                       sourceSize.height * (parent.width/sourceSize.width)
                width: sourceSize.width>sourceSize.height?
                       sourceSize.width*(parent.height/sourceSize.height):
                       parent.width;
            }
        }

        Image {
            id: mask
            smooth: true;
            visible:false
            source: "ui/uimask/roundMask.png"
            sourceSize:
                Qt.size(facade.toPx(300), facade.toPx(300))
        }

        OpacityMask {
            source: bug
            maskSource: mask
            anchors.fill:bug
        }
    }

    EmwStyle.DialogWindow{
        id: dialogAndroid;
    }
}
