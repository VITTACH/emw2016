import QtQuick 2.7
import QtQuick.Controls 2.0
import "EmwStyle" as EmwStyle

//--------------------------------------------------settingScreen.qml-------------------------------------------
Item {
    id: rootScreen

    ComboBox {
        visible: false

        model:ListModel {
            id:citysModel
            ListElement {
                text : ""
            }
        }
    }

    EmwStyle.NavigateDrawer
    {id: settingScreenDrawer}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Timer {
        running:true
        interval: 10
        onTriggered: {
            event_handler.getCitys()
            var city;
            while((city = event_handler.takeCity()) != "FATAL_ERROR")
            citysModel.append({
            text: city
            });
        }
    }

    ListView {
        id: listView
        width: parent.width
        spacing: facade.toPx(50)
        boundsBehavior: Flickable.StopAtBounds
        anchors {
            bottom: parent.bottom
            topMargin: facade.toPx(40)
            top:settingScreenNavbarMenu.bottom
        }

        model:  ListModel {
            id: settingsModel
                ListElement {
                    image: "ui/screensIcons/humanIconBlue.png"
                    plaseholder: "Имя";
                }
                ListElement {
                    image: "ui/screensIcons/humanIconBlue.png"
                    plaseholder: "Фамилия";
                }
                ListElement {
                    image: "ui/screensIcons/homeIconBlue.png"
                    plaseholder: "Ваш город";
                }
                ListElement {
                    image: "ui/screensIcons/badgeIconBlue.png"
                    plaseholder: "Должность";
                }
                ListElement {
                    image: "ui/screensIcons/companyIconBlue.png"
                    plaseholder: "Компания";
                }
                ListElement {
                    image: "ui/screensIcons/passIconBlue.png"
                    plaseholder: "Пароль";
                }
                ListElement {
                    image: "ui/screensIcons/passIconBlue.png"
                    plaseholder: "Повторите пароль";
                }
                ListElement {
                    image: "ui/screensIcons/phoneIconBlue.png"
                    plaseholder: "Номер мобильный";
                }
                ListElement {
                    image: ""
                    plaseholder: "Информация о себе";
                }
                ListElement {
                    image: ""
                    plaseholder: "Применить изменения";
                }
            }
        delegate: Item {
            width: parent.width
            height: index == 9?
                        facade.toPx(160):
                        (index == 8?
                             facade.toPx(60) + 3*facade.toPx(43):
                             facade.toPx(89)
                        )
            Button {
                text: plaseholder
                height: facade.toPx(110)
                visible: index == 9?1:0;

                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    leftMargin: 0.09 * parent.width;
                    rightMargin:0.09 * parent.width;
                }

                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name

                onClicked: {
                    if (loader.fields[0].length < 2) {
                        dialogAndroid.text = "Имя короче 2 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[1].length < 2) {
                        dialogAndroid.text = "Фамилия менее 2 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[7].length <11) {
                        dialogAndroid.text = "Укажите корректный номер"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[5].length < 5) {
                        dialogAndroid.text = "Пароль короче 5 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[5] != loader.fields[6])
                    {
                        dialogAndroid.text = "Ваши пароли не совпадают"
                        dialogAndroid.visible = true
                    }
                    else {
                        busyIndicator.visible = true
                        if (event_handler.updatedUsers(loader.fields[0],
                                                       loader.fields[1],
                                                       loader.fields[3],
                                                       loader.fields[4],
                                                       loader.fields[7],
                                                       loader.fields[8],
                                                       loader.fields[2]))
                        {
                            event_handler.setDescrip(loader.fields[8])
                            event_handler.setPost(loader.fields[3])
                            event_handler.setCity(loader.fields[2])
                            event_handler.setFamily(loader.fields[1])
                            event_handler.setCompany(loader.fields[4])
                            event_handler.setPass(loader.fields[5])
                            event_handler.setPhone(loader.fields[7])
                            event_handler.setLogin(loader.fields[0])
                            dialogAndroid.text = "Изменения были приняты"
                            dialogAndroid.visible=true
                        }
                        else {
                            dialogAndroid.text = "Изменения не приняты!";
                            dialogAndroid.visible=true
                        }
                        busyIndicator.visible = false;
                    }
                }

                background: Rectangle {
                    radius: facade.toPx(25)
                    color: parent.down?"#51587F":"#7680B1"
                }

                contentItem: Text {
                    color: parent.down?"#FFFFFF":"#FFFFFF"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment : Text.AlignVCenter
                    opacity: enabled?1:0.3
                    elide: Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }

            Item {
                height: index < 8?
                           facade.toPx(88):
                           facade.toPx(60) + 3*facade.toPx(43)
                visible:index < 9?1:0;
                anchors {
                    left: parent.left;
                    right:parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
                Image {
                    source: image
                    width: facade.toPx(sourceSize.width * 1.5)
                    height:facade.toPx(sourceSize.height* 1.5)
                }
                ComboBox {
                    id: comboBoxCitys;
                    model: citysModel;

                    height:facade.toPx(88)
                    visible:index== 2?1:0;

                    anchors {
                        left: parent.left;
                        right:parent.right
                        leftMargin: facade.toPx(30)
                    }

                    font.pixelSize: facade.doPx(24)
                    background:Rectangle{opacity:0}

                    Component.onCompleted: citysModel.clear()

                    onActivated: {
                    textFieldInRow.text=textAt(currentIndex);
                    popup.close()
                    }

                    contentItem: Text {
                        opacity: 0
                        font: comboBoxCitys.font
                        verticalAlignment: Text.AlignVCenter
                        leftPadding: comboBoxCitys.indicator.width + comboBoxCitys.spacing;
                    }

                    indicator: Canvas {
                        contextType: "2d"
                        width: facade.toPx(40)
                        height:facade.toPx(24)
                        x:comboBoxCitys.width - width - comboBoxCitys.rightPadding
                        y:comboBoxCitys.topPadding+(comboBoxCitys.availableHeight-height)/2

                        Connections {target:comboBoxCitys}

                        onPaint: {
                            context.reset();
                            context.moveTo(0,0);
                            context.lineTo(width, 0);
                            context.fillStyle = "#7680B1";
                            context.lineTo(width/2,height)
                            context.closePath();
                            context.fill();
                        }
                    }
                }
                TextField {
                    id: textFieldInRow
                    visible: index < 8?1:0;
                    height: facade.toPx(88)
                    onTextChanged: loader.fields[index] = text
                    echoMode: (index == 5 || index==6)? TextInput.Password:TextInput.Normal

                    Component.onCompleted: {
                        switch(index) {
                        case 0: {
                            loader.fields[index] = text = event_handler.getLogin();break;
                        }
                        case 1: {
                            loader.fields[index] = text = event_handler.getFamily(); break;
                        }
                        case 2: {
                            loader.fields[index] = text = event_handler.getCity(); break;
                        }
                        case 3: {
                            loader.fields[index] = text = event_handler.getPost(); break;
                        }
                        case 4: {
                            loader.fields[index] = text = event_handler.getCompany(); break;
                        }
                        case 5: {
                            loader.fields[index] = text = event_handler.getPass(); break;
                        }
                        case 7: {
                            loader.fields[index] = text = event_handler.getPhone(); break;
                        }
                        }
                    }
                    anchors {
                        left: parent.left
                        right: parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(30)
                    }
                    placeholderText: plaseholder

                    onFocusChanged: {
                        if(text.length==0 && index==7)
                        text = "+7"
                    }

                    inputMethodHints: (index == 7)? Qt.ImhFormattedNumbersOnly: Qt.ImhNone;

                    color: "#7680B1"
                    font.family: museoSansMidle.name
                    font.pixelSize: facade.doPx(28);
                    background:Rectangle{opacity:0}
                }
                Flickable {
                    visible: index==8? 1:0;
                    flickableDirection: Flickable.VerticalFlick;
                    anchors{
                        left: parent.left
                        right: parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(30)
                    }

                    height: facade.toPx(60) + 3*facade.toPx(43);

                    TextArea.flickable: TextArea {
                        placeholderText:plaseholder

                        onTextChanged: {
                            if(length > 200)
                            text = text.substring(0,200)
                            loader.fields[index] = text;
                            cursorPosition = length
                        }

                        Component.onCompleted: loader.fields[index] = text = event_handler.getDescrip()

                        color: "#7680B1"
                        wrapMode: TextEdit.Wrap
                        font.family: museoSansMidle.name
                        font.pixelSize: facade.doPx(26);
                        background:Rectangle{opacity: 0}
                    }
                }
            }

            Rectangle {//ебаное нижне подчеркивание
                height: 2
                color: "#7680B1"
                visible: index < 9?1:0
                anchors {
                    left: parent.left;
                    right:parent.right
                    bottom: parent.bottom
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
            }
        }
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(140)
        id: settingScreenNavbarMenu
        Image {
            y: -(height-facade.toPx(380))
            x: rootScreen.width/2-width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootScreen.width<rootScreen.height/2)?
                   (rootScreen.height/2):
                    sourceSize.height*(width/sourceSize.width)
            width: (rootScreen.width<rootScreen.height/2)?
                    sourceSize.width*
                    rootScreen.height/2/sourceSize.height:
                        rootScreen.width;
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton
            width: hambrButtonImage.width;
            height:hambrButtonImage.height

            background: Rectangle { opacity: 0; }

            onClicked: settingScreenDrawer.open()

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Text {
            color: "#FFFFFF"
            text:"Настройки"
            font.pixelSize: facade.doPx(28);
            font.family: museoSansMidle.name

            anchors {
                centerIn: parent
                leftMargin: profileDrawerButton.y + profileDrawerButton.width
            }
        }
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }
}
//-------------------------------------------------------------------------------------------------------------
