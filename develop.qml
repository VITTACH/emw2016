import QtQuick 2.7
import QtQuick.Controls 2.0
import"EmwStyle" as EmwStyle

Item {
    id: rootScreen
    property int vittach: 0

    EmwStyle.NavigateDrawer
    {id:developScreenDrawer}

    EmwStyle.ChatinListener
    {id:chatingecholistener}

    Rectangle {
        z: 1
        clip: true
        id: navbarMenu
        color: "#121317"
        width: parent.width;
        height: facade.toPx(400)

        Image {
            anchors.left: parent.left
            source: "ui/appLabs/LeftLine.png"
        }
        Image {
            anchors.right: parent.right
            source: "ui/appLabs/riGhtLin.png"
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton;
            width:2*hambrButtonImage.width;
            height:hambrButtonImage.height;
            background:Rectangle{opacity:0}

            onClicked:
                developScreenDrawer.open();

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Column {
            spacing: facade.toPx(30)
            anchors.centerIn: parent
            anchors.leftMargin: profileDrawerButton.y + profileDrawerButton.width

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                horizontalAlignment: Text.AlignHCenter
                anchors {
                  horizontalCenter: parent.horizontalCenter
                }

                text: "Евразийская Неделя\nМаркетинга 2016"
            }

            Button {
                background: Image {
                    id: logoVITTACH
                    width: facade.toPx(sourceSize.width / 1.1);
                    height:facade.toPx(sourceSize.height/ 1.1);
                    fillMode:Image.PreserveAspectFit
                    source: "ui/appLabs/applabS.png"
                }
                onClicked: {
                    vittach++
                    if (vittach >= 15) {
                        dialogAndroid.text = "Разработчик VITTACH\nСпециально для AppLabs LLC.\n\nСпасибо за использование нашего приложения"
                        dialogAndroid.visible = true;
                    }
                }
                width: logoVITTACH.width;
                height:logoVITTACH.height
            }

            Text {
                color: "#FFFFFF"
                wrapMode: Text.WordWrap
                font.pixelSize: facade.doPx(18.5)
                font.family: museoSansLight.name;
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Подготовили для вас это приложение";
            }
        }
    }

    ListView {
        id: listView
        width: parent.width
        spacing: facade.toPx(50)
        anchors {
            top:navbarMenu.bottom
            bottom: parent.bottom
            topMargin: facade.toPx(50)
        }
        boundsBehavior: Flickable.StopAtBounds

        model:  ListModel {
            id: registModel
                ListElement {
                    image: ""
                    plaseholder: "";
                }
                ListElement {
                    image: "ui/screensIcons/humanIconBlue.png"
                    plaseholder: "Имя";
                }
                ListElement {
                    image: "ui/screensIcons/phoneIconBlue.png"
                    plaseholder: "Телефон";
                }
                ListElement {
                    image: "ui/screensIcons/homeIconBlue.png"
                    plaseholder: "E-mail";
                }
                ListElement {
                    image: "ui/screensIcons/badgeIconBlue.png"
                    plaseholder: "Пожелания";
                }
                ListElement {
                    image: ""
                    plaseholder: "Отправить заявку";
                }
            }
        delegate: Item {
            width: parent.width
            height: index == 5?
                    facade.toPx(160):
                        (index == 4?
                             facade.toPx(60) + 3*facade.toPx(43):
                             (index == 0?
                                  appLabsText.implicitHeight:
                                    facade.toPx(89)
                             )
                        )

            Text {
                id: appLabsText
                color: "#7680B1"
                visible: index==0? 1:0;
                wrapMode: Text.WordWrap
                width: facade.toPx(575)
                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                //horizontalAlignment: Text.AlignJustify

                text: "Узнайте стоимость разработки мобильного приложения для Вашего бизнеса / Start-Up проекта.\n\nОставьте свою заявку в форме ниже!\n\nМы не делаем лучше или хуже конкурентов, мы делаем иначе!"
            }

            Button {
                text: plaseholder
                width: facade.toPx(575)
                height: facade.toPx(110)
                visible: index == 5? 1:0

                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }

                onClicked: {
                    if (loader.fields[1].length < 2) {
                        dialogAndroid.text = "Имя короче 2 символов"
                        dialogAndroid.visible = true
                    }
                    else
                    if (loader.fields[2].length <11) {
                        dialogAndroid.text = "Укажите корректный номер"
                        dialogAndroid.visible = true
                    }
                    else {
                        busyIndicator.visible = true
                        //Qt.openUrlExternally("http://app-labs.ru")
                        if(event_handler.applications(loader.fields[1],
                                                      loader.fields[2],
                                                      loader.fields[3],
                                                      loader.fields[4]))
                        {
                            dialogAndroid.text = "Благодарим\n\nВаша заявка уже принята в работу.\nМы свяжемся с вами в самое ближайшее время."
                            dialogAndroid.visible=true
                        }
                        else {
                            dialogAndroid.text = "Извините, нам не удалось отправить заявку :("
                            dialogAndroid.visible=true
                        }
                        busyIndicator.visible = false;
                    }
                }
                background: Rectangle {
                    radius:facade.toPx(25)
                    color:parent.down?"#64FF81":"#9CFF81"
                }
                contentItem: Text {
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment : Text.AlignVCenter
                    color:parent.down?"#000000":"#151515"
                    opacity: enabled?1:0.3
                    elide: Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }

            Item {
                height:index < 4?
                           facade.toPx(88):
                           facade.toPx(60) + 3*facade.toPx(43)
                visible:index > 0 && index < 5?1:0;
                anchors {
                    left: parent.left;
                    right:parent.right
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
                Image {
                    source: image
                    width: facade.toPx(sourceSize.width * 1.5)
                    height:facade.toPx(sourceSize.height* 1.5)
                }
                Flickable {
                    visible: index==4? 1:0;
                    flickableDirection: Flickable.VerticalFlick;
                    anchors{
                        left: parent.left
                        right: parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(30)
                    }

                    height: facade.toPx(60) + 3*facade.toPx(43);

                    TextArea.flickable: TextArea {
                        placeholderText:plaseholder

                        onTextChanged: loader.fields[index]=text

                        color: "#7680B1"
                        wrapMode: TextEdit.Wrap
                        font.family: museoSansMidle.name
                        font.pixelSize: facade.doPx(26);
                        background:Rectangle{opacity: 0}
                    }
                }
                TextField {
                    id: textFieldInRow
                    height:facade.toPx(88)
                    visible:index < 4?1:0;
                    placeholderText:plaseholder

                    anchors {
                        left: parent.left;
                        right:parent.right
                        rightMargin:0.09 * parent.width
                        leftMargin: 0.09 * parent.width + facade.toPx(30)
                    }

                    onTextChanged: {loader.fields[index] = text;}

                    Component.onCompleted: {
                        switch(index) {
                        case 1:
                            text = event_handler.getLogin() + " " + event_handler.getFamily();
                            break
                        case 2: {text = event_handler.getPhone(); break;}
                        case 3: {text = event_handler.getMail(); break;}
                        }
                    }

                    color: "#7680B1"

                    inputMethodHints: (index == 2)?
                    Qt.ImhFormattedNumbersOnly:Qt.ImhNone

                    onFocusChanged: {
                        if(text.length== 0 && index == 2)
                        text = "+7"
                    }

                    font.family: museoSansMidle.name
                    font.pixelSize: facade.doPx(28);
                    background:Rectangle{opacity:0}
                }
            }

            Rectangle {//ебаное нижне подчеркивание
                height: 2
                color: "#7680B1"
                visible: index > 0 && index < 5?1:0
                anchors {
                    left: parent.left;
                    right:parent.right
                    bottom: parent.bottom
                    leftMargin: 0.09 * parent.width
                    rightMargin:0.09 * parent.width
                }
            }
        }
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }
}
