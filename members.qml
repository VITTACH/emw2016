import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import "EmwStyle" as EmwStyle

Item {
    id: rootMembers
    property int pageIndex: 0

    EmwStyle.NavigateDrawer
    {id: membersScreenDrawer}

    EmwStyle.ChatinListener
    {id: chatingecholistener}

    Timer {
        id: upload
        running: true
        interval: 500
        onTriggered:{
            event_handler.getMembers(loader.pageMemberIndex, pageIndex)
            var i= 0;
            var days;
            var name;
            var post;
            var family;
            var avatar;
            var company;
            while(((avatar = event_handler.takeMember(0)) != "FATAL_ERROR")
                  &&((name = event_handler.takeMember(1)) != "FATAL_ERROR")
                  &&((post = event_handler.takeMember(2)) != "FATAL_ERROR")
                  &&((days = event_handler.takeMember(5)) != "FATAL_ERROR")
                  &&((family = event_handler.takeMember(4)) != "FATAL_ERROR")
                  &&((company= event_handler.takeMember(3)) != "FATAL_ERROR")
                  ) {
                    profileModel.append({
                        image: avatar,
                            target0: name,
                                target1: family,
                                    target2: post,
                                        target3: company,
                                            target4: days
                    });
                    event_handler.takeMember(-1)
                    if(i++==0)
                        if(pageIndex>0)
                            listView.positionViewAtEnd();
            }
            busyIndicator.visible = false

            showMoreMembersButton.visible= event_handler.isManyMembers()? 1:0
            showMoreMembersButton.height = event_handler.isManyMembers()?
                facade.toPx(15) + showMoreMembersButton.implicitHeight: 0
        }
    }

    ListView {
        Component.onCompleted: {
            profileModel.clear()
            busyIndicator.visible = true;
        }

        id: listView
        width: parent.width
        spacing: facade.toPx(10)
        boundsBehavior:Flickable.StopAtBounds
        maximumFlickVelocity: 0
        anchors{
            bottom: parent.bottom
            //topMargin: facade.toPx(-30)
            top: showMoreMembersButton.bottom
        }

        model:  ListModel {
            id: profileModel
                ListElement {
                    image: "";
                    target0: "";
                    target1: "";
                    target2: "";
                    target3: "";
                    target4: "";
                }
            }
        delegate:
            /*
            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                height: Math.max(bug.height,
                                     text1.implicitHeight +
                                     text2.implicitHeight +
                                     text3.implicitHeight +
                                     text4.implicitHeight
                                 ) + facade.toPx(60)
                LinearGradient {
                    anchors.fill: parent
                    start:Qt.point(0, 0)
                    end : Qt.point(0, facade.toPx(30))
                    gradient: Gradient {
                        GradientStop {
                            position: 0; color: "#51587F"
                        }
                        GradientStop {
                            position: 1; color: "#7680B1"
                        }
                    }
                }
                Column {
                    width: parent.width
                    anchors.bottom: parent.bottom;
                    */
                    Rectangle {
                        id: myrow
                        color: "#7680B1"
                        anchors.left: parent.left;
                        anchors.right:parent.right
                        x:listView.width / 2 - width / 2

                        MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                myrow.color = "#51587F";
                            }
                            onReleased: {
                                myrow.color = "#7680B1";
                            }
                            onPositionChanged: {
                                myrow.color = "#7680B1";
                            }

                            onClicked: {
                                var i=index;
                                loader.chatVariable = 0;
                                loader.currentChatIndex = index
                                event_handler.curMemberIndex(i)
                                loader.goTo("qrc:/memberProfile.qml")
                            }
                        }

                        width: parent.width
                        height: Math.max(bug.height,
                                         text1.implicitHeight +
                                         text2.implicitHeight +
                                         text3.implicitHeight +
                                         text4.implicitHeight
                                         ) + facade.toPx(40)

                        Item {
                            anchors.fill:parent
                            anchors.leftMargin: facade.toPx(20)
                            Rectangle {
                                id: bug
                                clip: true
                                smooth: true
                                visible: false
                                width: facade.toPx(200)
                                height:facade.toPx(200)
                                anchors.verticalCenter: parent.verticalCenter
                                Image {
                                    source: image
                                    height:sourceSize.width>sourceSize.height?
                                           facade.toPx(200):
                                           sourceSize.height * (parent.width/sourceSize.width)
                                    width: sourceSize.width>sourceSize.height?
                                           sourceSize.width*(parent.height/sourceSize.height):
                                           facade.toPx(200)
                                }
                            }

                            Image {
                                id: mask
                                smooth: true;
                                visible:false
                                source: "ui/uimask/roundMask.png"
                                sourceSize: Qt.size(bug.width,bug.height)
                            }

                            OpacityMask {
                                source: bug
                                maskSource: mask
                                anchors.fill: bug
                            }
                            Column {
                                width: parent.width/ 2;
                                spacing: facade.toPx(5)
                                anchors {
                                    right: parent.right
                                    rightMargin: facade.toPx(20)
                                    verticalCenter: parent.verticalCenter
                                }
                                Text {
                                    id: text1
                                    color:"#FFFFFF"
                                    styleColor: "black"
                                    style: Text.Raised;
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    font {
                                        bold: true
                                        pixelSize: facade.doPx(28);
                                        family: museoSansMidle.name
                                    }
                                    text: target0 + " " + target1;
                                }
                                Text {
                                    id: text2
                                    color: "#FFFFFF"
                                    styleColor: "black"
                                    style: Text.Raised;
                                    wrapMode: Text.Wrap
                                    width: parent.width - facade.toPx(20)
                                    font {
                                        pixelSize: facade.doPx(24);
                                        family: museoSansMidle.name
                                    }
                                    text: "\n"+target2;
                                }
                                Text {
                                    id: text3
                                    color: "#FFFFFF"
                                    styleColor: "black"
                                    style: Text.Raised;
                                    wrapMode: Text.Wrap
                                    width: parent.width - facade.toPx(20)
                                    font {
                                        pixelSize: facade.doPx(24);
                                        family: museoSansMidle.name
                                    }
                                    text: "в "+target3;
                                }
                                Text {
                                    id: text4
                                    visible: target4.length > 0?1:0
                                    color: "#FFFFFF"
                                    styleColor: "black"
                                    style: Text.Raised;
                                    wrapMode: Text.Wrap
                                    width: parent.width - facade.toPx(20)
                                    font {
                                        pixelSize: facade.doPx(24);
                                        family: museoSansMidle.name
                                    }
                                    text: "Дата выступления: " + target4;
                                }
                            }
                        }
                    }
                    /*
                    Rectangle {// подчеркивание
                        height: 2
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        y: index * myrow.height
                        border.color: "#B8BDD6"
                    }
                }
            }
            */
    }

    Rectangle {
        clip: true
        width: parent.width
        height: facade.toPx(140)
        id: membersScreenNavbarMenu
        Image {
            y: -(height-facade.toPx(280));
            x: rootMembers.width/2-width/2
            source: "ui/navbarMenu/navbarMenu.png"

            height:(rootMembers.width < rootMembers.height/2)?
                    rootMembers.height/2:
                    sourceSize.height*(width/sourceSize.width)
            width: (rootMembers.width < rootMembers.height/2)?
                    sourceSize.width*
                    (rootMembers.height/2 /sourceSize.height):
                        rootMembers.width
        }

        Button {
            x: facade.toPx(20)
            y: facade.toPx(41)
            id: profileDrawerButton
            width:2*hambrButtonImage.width;
            height:hambrButtonImage.height;
            background:Rectangle{opacity:0}

            onClicked: membersScreenDrawer.open()

            Image {
                id: hambrButtonImage
                width: facade.toPx(sourceSize.width * 2)
                height:facade.toPx(sourceSize.height* 2 - 6)
                fillMode: Image.PreserveAspectFit;
                source: "ui/buttons/hamButton.png"
            }
        }

        Column {
            anchors {
                centerIn: parent
                leftMargin: profileDrawerButton.y + profileDrawerButton.width
            }
            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(18);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("EMW 2016")
            }

            Text {
                color: "#FFFFFF"
                font.pixelSize: facade.doPx(24);
                font.family: museoSansMidle.name
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("Участники")
            }
        }
    }

    Button {
        width: parent.width
        id: showMoreMembersButton
        text:qsTr("Показать еще")
        anchors.top: membersScreenNavbarMenu.bottom

        font {
            underline: true
            pixelSize: facade.doPx(23);
            family: museoSansMidle.name
        }

        onClicked: {
            listView.positionViewAtEnd()
            busyIndicator.visible=true;
            loader.pageMemberIndex++
            pageIndex = pageIndex+1;
            upload.restart()
        }

        background: Rectangle{color: "#51587F"}

        contentItem: Text {
            color: "#FFFFFF"
            horizontalAlignment:Text.AlignRight
            verticalAlignment:Text.AlignVCenter
            style:Text.Raised
            styleColor:"black"
            text: parent.text
            font: parent.font
            padding: -8
        }
    }

    EmwStyle.DialogWindow {
        id: dialogAndroid
    }

    EmwStyle.BusyIndicator{
        id: busyIndicator
    }
}
